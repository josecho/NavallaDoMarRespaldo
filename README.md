# NavallaDoMarMX

GNU GENERAL PUBLIC LICENSE     Version 3, 29 June 2007
 
Aplicación Android  que actúa contra os servizos proporcionados pola API de MeteoSix : http://www.meteogalicia.es/web/proxectos/meteosix.action

A API KEY proporcionada no arquivo gradle.properties debe ser solicitada nas páxinas de MeteoSix para o correcto funcionamento 
da aplicación.

A documentación da API MeteoSix: 
http://www.meteogalicia.es/datosred/infoweb/meteo/proxectos/meteosix/API_MeteoSIX_gl.pdf

Esta aplicación pretende ser a primeira de toda unha serie de aplicacións relacionadas ca actividade marítima na costa galega.





