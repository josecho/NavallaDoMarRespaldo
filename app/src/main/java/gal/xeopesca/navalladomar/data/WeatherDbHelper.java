/*
 * NavallaDoMar
 * Copyright (C) 2016  Jose Luis Villaverde Balsa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 3 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gal.xeopesca.navalladomar.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import gal.xeopesca.navalladomar.data.WeatherContract.LocationEntry;
import gal.xeopesca.navalladomar.data.WeatherContract.NumericFcEntry;
import gal.xeopesca.navalladomar.data.WeatherContract.SkyStateEntry;
import gal.xeopesca.navalladomar.data.WeatherContract.DaysEntry;
/**
 * Manexa a base de datos local weather.db.
 */
public class WeatherDbHelper extends SQLiteOpenHelper {

    //No caso de cambiar o 'schema' , debemos incrementar a version da base de datos
    private static final int DATABASE_VERSION = 10;
    static final String DATABASE_NAME = "weather.db";

    public WeatherDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase sqLiteDatabase){
        super.onOpen(sqLiteDatabase);
        sqLiteDatabase.execSQL("PRAGMA foreign_keys=ON");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_LOCATION_TABLE = "CREATE TABLE " + LocationEntry.TABLE_NAME + " (" +
                // A provider isn't required to have a primary key, and it isn't required to use _ID
                // as the column name of a primary key if one is present. However, if you want to bind data
                // from a provider to a ListView, one of the column names has to be _ID
                //creamos a taboa para almacenar a localidade. A localidade buscase
                // en base ao string suministrado no location setting
                // o identificador da localidade
                //non pode ser AUTOINCREMENT, o valor e devolto polo servizo web
                LocationEntry._ID + " INTEGER PRIMARY KEY," +
                LocationEntry.COLUMN_LOC_NAME + " TEXT NOT NULL, " +
                LocationEntry.COLUMN_LOC_MUNICIPALITY + " TEXT NOT NULL, " +
                LocationEntry.COLUMN_LOC_PROVINCE + " TEXT NOT NULL, " +
                LocationEntry.COLUMN_LOC_TYPE + " TEXT NOT NULL, " +
                LocationEntry.COLUMN_COORD_LAT + " TEXT NOT NULL, " +
                LocationEntry.COLUMN_COORD_LONG + " TEXT NOT NULL " +
                " );";

        final String SQL_CREATE_NUMERIC_TABLE = "CREATE TABLE " + NumericFcEntry.TABLE_NAME + " (" +
                NumericFcEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                NumericFcEntry.COLUMN_NUM_ID_LOC + " INTEGER, " +
                NumericFcEntry.COLUMN_NUM_DAY + " DATE, " +
                // Set up the COLUMN_NUM_ID_LOC column as a foreign key to location table.
                " FOREIGN KEY (" + NumericFcEntry.COLUMN_NUM_ID_LOC + ") REFERENCES " +
                LocationEntry.TABLE_NAME + " (" + LocationEntry._ID + ") ON DELETE CASCADE " +
                // To assure the application have just one weather entry per day
                // per location, it's created a UNIQUE constraint with REPLACE strategy
                " UNIQUE (" + NumericFcEntry.COLUMN_NUM_DAY + ", " +
                NumericFcEntry.COLUMN_NUM_ID_LOC + ") ON CONFLICT REPLACE)";
                //NumericFcEntry.COLUMN_NUM_SKY_STATE + " TEXT , " +
               /* NumericFcEntry.COLUMN_NUM_TEMPERATURE + " INTEGER, " +
                NumericFcEntry.COLUMN_NUM_PRECIPITATION + " REAL, " +*/
                //Almacena dous numeros enteirios reais con dous decimais
                /*NumericFcEntry.COLUMN_NUM_WIND + " TEXT, " +
                NumericFcEntry.COLUMN_NUM_RELATIVE_HUMIDITY + " REAL, " +
                NumericFcEntry.COLUMN_NUM_CLOUD_AREA_FRACTION + " REAL, " +
                NumericFcEntry.COLUMN_NUM_AIR_PREASSURE_AT_SEA_LEVEL + " INTEGER, " +
                NumericFcEntry.COLUMN_NUM_SNOW_LEVEL + " INTEGER, " +
                NumericFcEntry.COLUMN_NUM_SEA_WATER_TEMPERTATURE + " INTEGER, " +
                NumericFcEntry.COLUMN_NUM_SIGNIFICATIVE_WAVE_HEIGHT + " REAL, " +
                NumericFcEntry.COLUMN_NUM_MEAN_WAVE_DIRECTION + " REAL, " +
                NumericFcEntry.COLUMN_NUM_RELATIVE_PEAK_PERIOD + " INTEGER, " +
                NumericFcEntry.COLUMN_NUM_SEA_WATER_SALINITY + " REAL " +*/
               // " );";

        final String SQL_CREATE_DAYS_TABLE = "CREATE TABLE " + DaysEntry.TABLE_NAME + " (" +
                DaysEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                DaysEntry.COLUMN_DAYS_ID_NUMERICFC + " INTEGER, " +
                DaysEntry.COLUMN_DAYS_BEGIN + " TEXT, " +
                DaysEntry.COLUMN_DAYS_END + " TEXT, " +
                " FOREIGN KEY (" + DaysEntry.COLUMN_DAYS_ID_NUMERICFC + ") REFERENCES " +
                NumericFcEntry.TABLE_NAME + " (" + NumericFcEntry._ID + ") ON DELETE CASCADE" +
                " );";

        final String SQL_CREATE_SKY_STATE_TABLE = "CREATE TABLE " + SkyStateEntry.TABLE_NAME + " (" +
                SkyStateEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                SkyStateEntry.COLUMN_SKY_ID_DAY + " INTEGER, " +
                SkyStateEntry.COLUMN_SKY_STATE_VALUE + " TEXT ," +
                SkyStateEntry.COLUMN_SKY_TIME_INSTANT + " TEXT, " +
                SkyStateEntry.COLUMN_SKY_MODEL_RUN + " TEXT, " +
                SkyStateEntry.COLUMN_SKY_ICON_URL + " TEXT, " +
                " FOREIGN KEY (" +  SkyStateEntry.COLUMN_SKY_ID_DAY + ") REFERENCES " +
                 DaysEntry.TABLE_NAME + " (" + DaysEntry._ID + ") ON DELETE CASCADE" +
                " );";

        sqLiteDatabase.execSQL(SQL_CREATE_LOCATION_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_NUMERIC_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_DAYS_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_SKY_STATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        // Note that this only fires if you change the version number for your database.
        // It does NOT depend on the version number for your application.
        // If you want to update the schema without wiping data, commenting out the next 2 lines
        // should be your top priority before modifying this method.
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + SkyStateEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DaysEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + NumericFcEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + LocationEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
