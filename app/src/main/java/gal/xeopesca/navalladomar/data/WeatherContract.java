/*
 * NavallaDoMar
 * Copyright (C) 2016  Jose Luis Villaverde Balsa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 3 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gal.xeopesca.navalladomar.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Define os nomes das taboas e columnas da bd weather.
 */
public class WeatherContract {
    // Unha content URI identifica datos no proveedor de contidos (content provider)
    // The "Content authority" e o nome enteiro do content provider, similar a relazon
    // entre o nome dun dominio e a sua website.E boa idea empregar como nome o nome da xerarquia
    //no de paquetes que conten a app, deste modo garantizamos que o nome e unico no dispositivo
    public static final String CONTENT_AUTHORITY = "gal.xeopesca.navalladomar";
    // Usamos CONTENT_AUTHORITY para crear a base de todas as URI's que as apps empregaran para
    // establecer contacto co content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    // Possible paths (appended to base content URI for possible URI's)
    // For instance, content://gal.xeopesca.navalladomar/location/ is a valid path for
    // looking at location data. content://gal.xeopesca.navalladomar/givemeroot/ will fail,
    // as the ContentProvider hasn't been given any information on what to do with "givemeroot".
    // At least, let's hope not.  Don't be that dev, reader.  Don't be that dev.
    // Un proveedor normalmente ten un path por cada taboa que expon.
    public static final String PATH_LOCATION = "location";
    public static final String PATH_NUMERICFC = "getNumericForecastInfo";
    public static final String PATH_DAYS = "days";
    public static final String PATH_SKYSTATE = "skystate";

    /* Clase interna que define os contidos da taboa location */
    public static final class LocationEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_LOCATION).build();
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LOCATION;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LOCATION;

        public static final String TABLE_NAME = "location";

        // o _ID esta definido na interface BaseColumns
        //public static final String COLUMN_LOC_ID = "location_id";
        public static final String COLUMN_LOC_NAME = "name";
        public static final String COLUMN_LOC_MUNICIPALITY = "municipality";
        public static final String COLUMN_LOC_PROVINCE = "province";
        public static final String COLUMN_LOC_TYPE = "type";
        public static final String COLUMN_COORD_LAT = "coord_lat";
        public static final String COLUMN_COORD_LONG = "coord_long";

        public static Uri buildLocationUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        //devolve a Uri contento o string empregrado na busqueda de localidades
        public static Uri buildLocationSetting(String locationSetting) {
            return CONTENT_URI.buildUpon().appendPath(locationSetting).build();
        }

        //devolve a Uri contendo o string de busqueda de localidades e a location_id
        public static Uri buildNameLocationId(String locationSetting, String localidade_id) {
            return CONTENT_URI.buildUpon().appendPath(locationSetting)
                    .appendPath(localidade_id).build();
        }

        //extrae da uri o string empregado na busqueda de localidades e o devolve
        public static String getLocationSettingFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        //extrae da uri o identificador da localidade, location_id e o devolve
        public static String getLocationIdSettingFromUri(Uri uri) {
            return uri.getPathSegments().get(2);
        }
    }

    /* Clase interna que define os contidos da taboa numericfc */
    public static final class NumericFcEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_NUMERICFC).build();
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_NUMERICFC;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_NUMERICFC;

        public static final String TABLE_NAME = "numericfc";

        // o _ID esta definido na interface BaseColumns
        // Column with the foreign key into the location table.
        public static final String COLUMN_NUM_ID_LOC = "idLocation";
        public static final String COLUMN_NUM_DAY = "day";
        //public static final String COLUMN_NUM_SKY_STATE = "sky_state";

        /*public static final String COLUMN_NUM_TEMPERATURE = "temperature";
        public static final String COLUMN_NUM_PRECIPITATION = "precipitation_amount";
        public static final String COLUMN_NUM_WIND = "wind";
        public static final String COLUMN_NUM_RELATIVE_HUMIDITY = "relative_himidity";
        public static final String COLUMN_NUM_CLOUD_AREA_FRACTION = "cloud_area_fraction";
        public static final String COLUMN_NUM_AIR_PREASSURE_AT_SEA_LEVEL = "air_pressure_at_sea_level";
        public static final String COLUMN_NUM_SNOW_LEVEL = "snow_level";
        public static final String COLUMN_NUM_SEA_WATER_TEMPERTATURE = "sea_water_tempertature";
        public static final String COLUMN_NUM_SIGNIFICATIVE_WAVE_HEIGHT = "significative_wave_height";
        public static final String COLUMN_NUM_MEAN_WAVE_DIRECTION = "mean_wave_direction";
        public static final String COLUMN_NUM_RELATIVE_PEAK_PERIOD = "relative_peak_period";
        public static final String COLUMN_NUM_SEA_WATER_SALINITY = "sea_water_salinity";*/

        public static Uri buildNumericFcUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri builNumericPredictionwithIdLocation(String idLocation) {
            return CONTENT_URI.buildUpon().appendPath(idLocation).build();
        }

        //extrae da uri o identificador da localidade, location_id e o devolve
        public static String getLocationIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    /* Clase interna que define os contidos da taboa numericfc */
    public static final class DaysEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_DAYS).build();
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_DAYS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_DAYS;

        public static final String TABLE_NAME = "days";
        // o _ID esta definido na interface BaseColumns
        // Actua como fk contra a taboa numericfc.
        public static final String COLUMN_DAYS_ID_NUMERICFC = "idNumericFC";
        public static final String COLUMN_DAYS_DATE = "date";
        public static final String COLUMN_DAYS_BEGIN = "beginTimeInstant";
        public static final String COLUMN_DAYS_END = "endTimeInstant";
        // identificador en SkyStateEntry
        //public static final String COLUMN_DAYS_SKY_STATE_ID = "skystateId";


        public static Uri buildDaysUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri builDayswithIdNumericFc(String IdNumericFc) {
            return CONTENT_URI.buildUpon().appendPath(IdNumericFc).build();
        }

        //extrae da uri o identificador da numericFc e o devolve
        public static String getNumericFcIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

    }

    /* Clase interna que define os contidos da taboa numericfc */
    public static final class SkyStateEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_SKYSTATE).build();
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SKYSTATE;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SKYSTATE;

        public static final String TABLE_NAME = "skystate";
        // o _ID esta definido na interface BaseColumns
        // foreign key to DaysEntry table.
        public static final String COLUMN_SKY_ID_DAY = "idDay";
        public static final String COLUMN_SKY_STATE_VALUE = "value";
        public static final String COLUMN_SKY_TIME_INSTANT = "timeInstant";
        public static final String COLUMN_SKY_MODEL_RUN = "modelRun";
        public static final String COLUMN_SKY_ICON_URL = "iconURL";


        public static Uri buildSkyStateUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri builSkyStateWithIdDay(String idDay) {
            return CONTENT_URI.buildUpon().appendPath(idDay).build();
        }

        //extrae da uri o identificador do Day e o devolve
        public static String getDayIdFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

    }



}
