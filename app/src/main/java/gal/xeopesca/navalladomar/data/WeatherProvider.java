/*
 * NavallaDoMar
 * Copyright (C) 2016  Jose Luis Villaverde Balsa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 3 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gal.xeopesca.navalladomar.data;

import android.annotation.TargetApi;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

// Se queremos dar acceso a este provider desde outra aplicacion
// esta debe requerir permisos especificos no arquivo do manifesto.
public class WeatherProvider extends ContentProvider {

    // A URI Matcher empregada polo proveedor de contidos (content provider).
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private WeatherDbHelper mOpenHelper;

    static final int LOCATION = 100;
    static final int LOCATION_WITH_NAME = 101;
    static final int LOCATION_WITH_NAME_AND_LOC_ID = 102;
    static final int NUMERIC_PREDICTION = 300;
    static final int NUMERIC_PREDICTION_WITH_LOC_ID = 301;
    static final int DAYS = 400;
    static final int DAYS_WITH_NUMERICFC_ID = 401;
    static final int SKYSTATE = 500;
    static final int SKYSTATE_WITH_DAY_ID = 501;

    private static final SQLiteQueryBuilder sLocalidadesByLocationSettingQueryBuilder;
    private static final SQLiteQueryBuilder sNumericPredictionQueryBuilder;
    private static final SQLiteQueryBuilder sDaysByNumericPredictionQueryBuilder;
    private static final SQLiteQueryBuilder sSkyStateByDayQueryBuilder;

    static{
        sLocalidadesByLocationSettingQueryBuilder = new SQLiteQueryBuilder();
        sLocalidadesByLocationSettingQueryBuilder.setTables(
                WeatherContract.LocationEntry.TABLE_NAME);
        sNumericPredictionQueryBuilder = new SQLiteQueryBuilder();
        sNumericPredictionQueryBuilder.setTables(
                WeatherContract.NumericFcEntry.TABLE_NAME + " INNER JOIN " +
                        WeatherContract.LocationEntry.TABLE_NAME +
                        " ON " + WeatherContract.NumericFcEntry.TABLE_NAME +
                        "." + WeatherContract.NumericFcEntry.COLUMN_NUM_ID_LOC +
                        " = " + WeatherContract.LocationEntry.TABLE_NAME +
                        "." + WeatherContract.LocationEntry._ID);

        sDaysByNumericPredictionQueryBuilder = new SQLiteQueryBuilder();
        sDaysByNumericPredictionQueryBuilder.setTables(
                WeatherContract.NumericFcEntry.TABLE_NAME + " INNER JOIN " +
                        WeatherContract.DaysEntry.TABLE_NAME +
                        " ON " + WeatherContract.NumericFcEntry.TABLE_NAME +
                        "." + WeatherContract.NumericFcEntry._ID +
                        " = " + WeatherContract.DaysEntry.TABLE_NAME +
                        "." + WeatherContract.DaysEntry.COLUMN_DAYS_ID_NUMERICFC
        );
        sSkyStateByDayQueryBuilder = new SQLiteQueryBuilder();
        sSkyStateByDayQueryBuilder.setTables(
                WeatherContract.DaysEntry.TABLE_NAME + " INNER JOIN " +
                        WeatherContract.SkyStateEntry.TABLE_NAME +
                        " ON " + WeatherContract.DaysEntry.TABLE_NAME +
                        "." + WeatherContract.DaysEntry._ID +
                        " = " + WeatherContract.SkyStateEntry.TABLE_NAME +
                        "." + WeatherContract.SkyStateEntry.COLUMN_SKY_ID_DAY
        );

    }


    //location.location_setting = like ?
    private static final String sLocationSettingSelection =
            WeatherContract.LocationEntry.TABLE_NAME +
                    "." + WeatherContract.LocationEntry.COLUMN_LOC_NAME + "  like ?";

    private static final String sLocationIdSelection =
            WeatherContract.LocationEntry.TABLE_NAME +
                    "." + WeatherContract.LocationEntry._ID + "  = ?";

    // Prediccion numerica en funcion do id_localidade
    private static final String sNumericPredictionLocationIdSelection =
            WeatherContract.NumericFcEntry.TABLE_NAME +
                    "." + WeatherContract.NumericFcEntry.COLUMN_NUM_ID_LOC + "  = ?";

    // Dias en funcion do idNumericFC
    private static final String sDaysNumericPredictionIdSelection =
            WeatherContract.DaysEntry.TABLE_NAME +
                    "." + WeatherContract.DaysEntry.COLUMN_DAYS_ID_NUMERICFC + "  = ?";

    // SkyState en funcion do idDay
    private static final String sSkyStateDaysIdSelection =
            WeatherContract.SkyStateEntry.TABLE_NAME +
                    "." + WeatherContract.SkyStateEntry.COLUMN_SKY_ID_DAY + "  = ?";

    //Devolve todas as localidades que conteñan o string de locationSetting
    private Cursor getLocalidadesByLocationSetting(
            Uri uri, String[] projection, String sortOrder) {
        String locationSetting = WeatherContract.LocationEntry.getLocationSettingFromUri(uri);
        return sLocalidadesByLocationSettingQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                sLocationSettingSelection,
                new String[]{"%" + locationSetting + "%"},
                null,
                null,
                sortOrder
        );
    }

    //For example, to retrieve a row whose _ID is 4 from user dictionary, you can use this content URI:
    //Uri singleUri = ContentUris.withAppendedId(UserDictionary.Words.CONTENT_URI,4);
    //Devolve unha localidade en funcion do localidade_id
    private Cursor getLocalidadeByLocationId(Uri uri, String[] projection, String sortOrder) {
        String locationId = WeatherContract.LocationEntry.getLocationIdSettingFromUri(uri);
        return sLocalidadesByLocationSettingQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                sLocationIdSelection,
                new String[]{locationId},
                null,
                null,
                sortOrder
        );
    }

    //Devolve unha prediccion numerica en funcion do localidade_id
    private Cursor getNumericPredictionByLocationId(Uri uri, String[] projection, String sortOrder) {
        String locationId = WeatherContract.NumericFcEntry.getLocationIdFromUri(uri);
        return sNumericPredictionQueryBuilder.query(
                mOpenHelper.getReadableDatabase(),
                projection ,
                sNumericPredictionLocationIdSelection,
                new String[]{locationId},
                null,
                null,
                sortOrder
        );
    }

    //Devolve os dias en funcion da numericFcId
    private Cursor getDaysBynumericFcId(Uri uri, String[] projection, String sortOrder) {
        String numericFcId = WeatherContract.DaysEntry.getNumericFcIdFromUri(uri);
        return sDaysByNumericPredictionQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                sDaysNumericPredictionIdSelection,
                new String[]{numericFcId},
                null,
                null,
                sortOrder
        );
    }

    //Devolve o skyState en funcion do dayId
    private Cursor getSkyStateByDayId(Uri uri, String[] projection, String sortOrder) {
        String dayId = WeatherContract.SkyStateEntry.getDayIdFromUri(uri);
        return sSkyStateByDayQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                projection,
                sSkyStateDaysIdSelection,
                new String[]{dayId},
                null,
                null,
                sortOrder
        );
    }


    /*
        Creamos o UriMatcher. O UriMatcher buscara o semellante a cada una das URI LOCATION,
        LOCATION_WITH_NAME_AND_LOC_ID e LOCATION_WITH_NAME (constantes integer) definidas ao
        principio desta clase.
        Testeado desde o metodo testUriMatcherdo situado dentro do test TestUriMatcher.
     */
    static UriMatcher buildUriMatcher() {
        // Todas as rutas engadidas ao UriMatcher teñen o seu correspondente codigo o cal e
        // devolto cando unha coincidencia e atopada.
        // O codigo pasado no constructor representa o codigo a devolver ao root URI
        // It's common to use NO_MATCH as the code for this case.
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = WeatherContract.CONTENT_AUTHORITY;
        // Por cada URI que engadimos, asociamos o codigo correspondente (100,101,etc...).
        // # follow by a number    * follow by a number
        matcher.addURI(authority, WeatherContract.PATH_LOCATION,LOCATION);
        matcher.addURI(authority, WeatherContract.PATH_LOCATION + "/*",LOCATION_WITH_NAME);
        matcher.addURI(authority, WeatherContract.PATH_LOCATION + "/*/#", LOCATION_WITH_NAME_AND_LOC_ID);
        matcher.addURI(authority, WeatherContract.PATH_NUMERICFC,NUMERIC_PREDICTION);
        matcher.addURI(authority, WeatherContract.PATH_NUMERICFC + "/*",NUMERIC_PREDICTION_WITH_LOC_ID);
        matcher.addURI(authority, WeatherContract.PATH_DAYS,DAYS);
        matcher.addURI(authority, WeatherContract.PATH_DAYS + "/#",DAYS_WITH_NUMERICFC_ID);
        matcher.addURI(authority, WeatherContract.PATH_SKYSTATE,SKYSTATE);
        matcher.addURI(authority, WeatherContract.PATH_SKYSTATE + "/#",SKYSTATE_WITH_DAY_ID);
        // Devolucion do novo matcher!
        return matcher;
    }

    /*
         Creamos un novo WeatherDbHelper para empregar aqui.
     */
    @Override
    public boolean onCreate() {
        mOpenHelper = new WeatherDbHelper(getContext());
        return true;
    }

    //Testeado desde testGetType en TestProvider
    @Override
    public String getType(Uri uri) {
        // Empregamos o UriMatcher para determinar o tipo de URI.
        //CONTENT_ITEM_TYPE return a simple record
        //CONTENT_TYPE return multiple records (a directory)
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case LOCATION_WITH_NAME_AND_LOC_ID:
                return WeatherContract.LocationEntry.CONTENT_ITEM_TYPE;
            case LOCATION_WITH_NAME:
                return WeatherContract.LocationEntry.CONTENT_TYPE;
            case LOCATION:
                return WeatherContract.LocationEntry.CONTENT_TYPE;
            case NUMERIC_PREDICTION:
                return WeatherContract.NumericFcEntry.CONTENT_TYPE;
            case NUMERIC_PREDICTION_WITH_LOC_ID:
                return WeatherContract.NumericFcEntry.CONTENT_ITEM_TYPE;
            case DAYS:
                return WeatherContract.DaysEntry.CONTENT_TYPE;
            case DAYS_WITH_NUMERICFC_ID:
                return WeatherContract.DaysEntry.CONTENT_ITEM_TYPE;
            case SKYSTATE:
                return WeatherContract.SkyStateEntry.CONTENT_TYPE;
            case SKYSTATE_WITH_DAY_ID:
                return WeatherContract.SkyStateEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        //dada unha URI, determinaremos o tipo de peticion(request),
        // executando a consulta(query) correspondente
        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {
            //location/*/*
            case LOCATION_WITH_NAME_AND_LOC_ID: {
                retCursor = getLocalidadeByLocationId(uri, projection, sortOrder);
                break;
            }
            //location/*
            case LOCATION_WITH_NAME: {
                retCursor = getLocalidadesByLocationSetting(uri, projection, sortOrder);
                break;
            }
            //location/
            case LOCATION: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        WeatherContract.LocationEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            //getNumericForecastInfo
            case NUMERIC_PREDICTION: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        WeatherContract.NumericFcEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            //getNumericForecastInfo
            case NUMERIC_PREDICTION_WITH_LOC_ID: {
                retCursor = getNumericPredictionByLocationId(uri, projection, sortOrder);
                break;
            }
            //Days/
            case DAYS: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        WeatherContract.DaysEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            //Days/*
            case DAYS_WITH_NUMERICFC_ID: {
                retCursor = getDaysBynumericFcId(uri, projection, sortOrder);
                break;
            }
            //SkyState/
            case SKYSTATE: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        WeatherContract.SkyStateEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case SKYSTATE_WITH_DAY_ID: {
                retCursor = getSkyStateByDayId(uri, projection, sortOrder);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;
        switch (match) {
            case LOCATION: {
                //devolve o row ID da nova inserccion, ou -1 si ocorre un erro.
                long _id = db.insert(WeatherContract.LocationEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = WeatherContract.LocationEntry.buildLocationUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case NUMERIC_PREDICTION: {
                //devolve o row ID da nova inserccion, ou -1 si ocorre un erro.
                long _id = db.insert(WeatherContract.NumericFcEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = WeatherContract.NumericFcEntry.buildNumericFcUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case DAYS: {
                //TODO antes de insertar chequear si existe
                //devolve o row ID da nova inserccion, ou -1 si ocorre un erro.
                long _id = db.insert(WeatherContract.DaysEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = WeatherContract.DaysEntry.buildDaysUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case SKYSTATE: {
                //TODO antes de insertar chequear si existe
                //devolve o row ID da nova inserccion, ou -1 si ocorre un erro.
                long _id = db.insert(WeatherContract.SkyStateEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = WeatherContract.SkyStateEntry.buildSkyStateUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        // Emprega o uriMatcher para identificar as LOCATION URI's que imos a manexar.
        // Se non identifica ningunha lanza unha UnsupportedOperationException.
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;
        //To remove all rows and get a count pass "1" as the whereClause
        if (null == selection) selection = "1";
        // A null value deletes all rows.  In my implementation of this, I only notified
        // the uri listeners (using the content resolver) if the rowsDeleted != 0 or the selection
        // is null.
        switch (match) {
            case LOCATION:
                rowsDeleted = db.delete(
                        WeatherContract.LocationEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case NUMERIC_PREDICTION:
                rowsDeleted = db.delete(
                        WeatherContract.NumericFcEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case DAYS:
                rowsDeleted = db.delete(
                        WeatherContract.DaysEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case SKYSTATE:
                rowsDeleted = db.delete(
                        WeatherContract.SkyStateEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Oh, and you should notify the listeners here.
        // Because a null deletes all rows
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        // This is a lot like the delete function.  We return the number of rows impacted
        // by the update.
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;
        switch (match) {
            case LOCATION:
                rowsUpdated = db.update(WeatherContract.LocationEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case NUMERIC_PREDICTION:
                rowsUpdated = db.update(WeatherContract.NumericFcEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case DAYS:
                rowsUpdated = db.update(WeatherContract.DaysEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case SKYSTATE:
                rowsUpdated = db.update(WeatherContract.SkyStateEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case DAYS:
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(WeatherContract.DaysEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            default:
                return super.bulkInsert(uri, values);
        }
    }

    // You do not need to call this method. This is a method specifically to assist the testing
    // framework in running smoothly. You can read more at:
    // http://developer.android.com/reference/android/content/ContentProvider.html#shutdown()
    @Override
    @TargetApi(11)
    public void shutdown() {
        mOpenHelper.close();
        super.shutdown();
    }
}