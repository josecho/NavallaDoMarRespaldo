package gal.xeopesca.navalladomar;


import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import gal.xeopesca.navalladomar.data.WeatherContract;


/**
 * A simple {@link Fragment} subclass.
 */
public class NumericPrediccionsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String LOG_TAG = NumericPrediccionsFragment.class.getSimpleName();
    private static final int PREDICCIONS_NUM_LOADER = 0;



    private static final String[] PREDICCIONS_NUM_COLUMNS = {
            WeatherContract.NumericFcEntry.TABLE_NAME + "." + WeatherContract.NumericFcEntry._ID,
            WeatherContract.NumericFcEntry.COLUMN_NUM_ID_LOC,
            WeatherContract.NumericFcEntry.COLUMN_NUM_DAY
            /*WeatherContract.NumericFcEntry.COLUMN_NUM_SKY_STATE,
            WeatherContract.NumericFcEntry.COLUMN_NUM_TEMPERATURE,
            WeatherContract.NumericFcEntry.COLUMN_NUM_PRECIPITATION,
            WeatherContract.NumericFcEntry.COLUMN_NUM_WIND,
            WeatherContract.NumericFcEntry.COLUMN_NUM_RELATIVE_HUMIDITY,
            WeatherContract.NumericFcEntry.COLUMN_NUM_CLOUD_AREA_FRACTION,
            WeatherContract.NumericFcEntry.COLUMN_NUM_AIR_PREASSURE_AT_SEA_LEVEL,
            WeatherContract.NumericFcEntry.COLUMN_NUM_SNOW_LEVEL,
            WeatherContract.NumericFcEntry.COLUMN_NUM_SEA_WATER_TEMPERTATURE,
            WeatherContract.NumericFcEntry.COLUMN_NUM_SIGNIFICATIVE_WAVE_HEIGHT,
            WeatherContract.NumericFcEntry.COLUMN_NUM_MEAN_WAVE_DIRECTION,
            WeatherContract.NumericFcEntry.COLUMN_NUM_RELATIVE_PEAK_PERIOD,
            WeatherContract.NumericFcEntry.COLUMN_NUM_SEA_WATER_SALINITY,*/
    };

    // Istos indices estan asociados a PREDICCIONS_NUM_COLUMNS.Se PREDICCIONS_NUM_COLUMNS cambia, os indices
    // deben ser cambiados.
    static final int COL_LOC_ID  = 0;
    static final int COLUMN_NUM_ID_LOC = 1;
    static final int COLUMN_LOC_DAY = 2;

   /* static final int COLUMN_NUM_SKY_STATE = 2;
    static final int COLUMN_NUM_TEMPERATURE = 3;
    static final int COLUMN_NUM_PRECIPITATION = 4;
    static final int COLUMN_NUM_WIND = 5;
    static final int COLUMN_NUM_RELATIVE_HUMIDITY = 6;
    static final int COLUMN_NUM_CLOUD_AREA_FRACTION = 7;
    static final int COLUMN_NUM_AIR_PREASSURE_AT_SEA_LEVEL = 8;
    static final int COLUMN_NUM_SNOW_LEVEL = 9;
    static final int COLUMN_NUM_SEA_WATER_TEMPERTATURE = 10;
    static final int COLUMN_NUM_SIGNIFICATIVE_WAVE_HEIGHT = 11;
    static final int COLUMN_NUM_MEAN_WAVE_DIRECTION = 12;
    static final int COLUMN_NUM_RELATIVE_PEAK_PERIOD = 13;
    static final int COLUMN_NUM_SEA_WATER_SALINITY = 14;*/

    private NumericPredicctionsAdapter mNumericPredictionAdapter;

    public NumericPrediccionsFragment() {
        // Required empty public constructor
    }

    //TODO Necesario?
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //para que este fragmento poda manexar eventos de menu
        // A primeira vez cargamos automaticamente, logo e necesario executar refresh(),

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Expande o menu; engade items ao action bar si esta presente.
        // refres()

        inflater.inflate(R.menu.numericpredictionfragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            updateNumericPredictions();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    private void updateNumericPredictions() {
        FetchNumericPredictionsTask numericPredictionsTask = new FetchNumericPredictionsTask(getActivity());
        //TODO revisar como conseguir el idlocalidade
        //Uri localidadeForLocationNameAndIdUri = getActivity().getIntent().getData();
        //String idlocation = WeatherContract.NumericFcEntry.getLocationIdFromUri(localidadeForLocationNameAndIdUri);
        String idlocation = getActivity().getIntent().getStringExtra("localidade_id");
        numericPredictionsTask.execute(idlocation);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //O CursorAdapter colle datos do cursor e os posiciona no ListView.
        mNumericPredictionAdapter = new NumericPredicctionsAdapter(getActivity(), null, 0);
        View rootView = inflater.inflate(R.layout.fragment_numericprediccions, container, false);
        //Consigue unha referencia do ListView existente lo 'layout' e asocialle o adaptador.
        ListView listView = (ListView) rootView.findViewById(R.id.listview_numeric_prediction);
        listView.setAdapter(mNumericPredictionAdapter);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstance){
        getLoaderManager().initLoader(PREDICCIONS_NUM_LOADER, null, this);
        super.onActivityCreated(savedInstance);
    }



    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.v(LOG_TAG, "In onCreateLoader");
        Intent intent = getActivity().getIntent();
        if (intent == null) {
            return null;
        }
        //TODO mirar uri
        //Uri localidadeForLocationNameAndIdUri = intent.getData();
        String id_localidade = intent.getExtras().getString("localidade_id");
        Uri prediccionsForLocationUri = WeatherContract.NumericFcEntry.builNumericPredictionwithIdLocation(id_localidade);
        return new CursorLoader(
                getActivity(),
                prediccionsForLocationUri,
                PREDICCIONS_NUM_COLUMNS,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
