package gal.xeopesca.navalladomar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Created by tx on 4/1/16.
 */
public class NumericPredictionsActivity extends AppCompatActivity {

    private final String LOG_TAG = NumericPredictionsActivity.class.getSimpleName();

    private final String NUMERIC_PREDICTIONSFRAGMENT_TAG = "PFTAG";
    private String mLocation;
    private Uri uriback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLocation = Utility.getPreferredLocation(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numericpredictions);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new NumericPrediccionsFragment(), NUMERIC_PREDICTIONSFRAGMENT_TAG)
                    .commit();
        }

        //TODO De momento deshabilito boton back
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        //datos pasados desde DetailActivy, os necesita no retorno
        //Intent intent = this.getIntent();
        //uriback = intent.getData();
        //Log.v(LOG_TAG, "**********************" + uriback.toString());


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    /*@Override
    protected void onResume() {
        super.onResume();
        String location = Utility.getPreferredLocation(this);
        // update the location in our second pane using the fragment manager
        if (location != null && !location.equals(mLocation)) {
            ForecastFragment ff = (ForecastFragment)getSupportFragmentManager().findFragmentByTag(NUMERIC_PREDICTIONSFRAGMENT_TAG);
            if ( null != ff ) {
                ff.onLocationChanged();
            }
            mLocation = location;
        }
    }*/


}
