package gal.xeopesca.navalladomar;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by tx on 4/1/16.
 */
public class NumericPredicctionsAdapter extends CursorAdapter {


    public NumericPredicctionsAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_numeric_prediction, parent, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // our view is pretty simple here --- just a text view
        // we'll keep the UI functional with a simple (and slow!) binding.
        TextView tv = (TextView) view;
        tv.setText(convertCursorRowToUXFormat(cursor));
    }

    /*
        This is ported from FetchWeatherTask --- but now we go straight from the cursor to the
        string.
     */
    String convertCursorRowToUXFormat(Cursor cursor) {
        return cursor.getString(NumericPrediccionsFragment.COL_LOC_ID) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_ID_LOC) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_LOC_DAY);
                /*" - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_SKY_STATE) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_TEMPERATURE) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_PRECIPITATION) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_WIND) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_RELATIVE_HUMIDITY) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_CLOUD_AREA_FRACTION) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_AIR_PREASSURE_AT_SEA_LEVEL) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_SNOW_LEVEL) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_SEA_WATER_TEMPERTATURE) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_SIGNIFICATIVE_WAVE_HEIGHT) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_MEAN_WAVE_DIRECTION) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_RELATIVE_PEAK_PERIOD) +
                " - " + cursor.getString(NumericPrediccionsFragment.COLUMN_NUM_SEA_WATER_SALINITY);*/


    }

}
