/*
 * NavallaDoMar
 * Copyright (C) 2016  Jose Luis Villaverde Balsa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 3 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gal.xeopesca.navalladomar;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.app.LoaderManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import gal.xeopesca.navalladomar.data.WeatherContract;

/**
 * Encapsulates fetching the forecast and displaying it as a {@link ListView} layout.
 */
public class ForecastFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int LOCALIDADE_LOADER = 0;

    // For the forecast view we're showing only a small subset of the stored data.
    // Specify the columns we need.
    private static final String[] LOCALIDADE_COLUMNS = {
            // In this case the id needs to be fully qualified with a table name, since
            // the content provider joins the location & weather tables in the background
            // (both have an _id column)
            // On the one hand, that's annoying.  On the other, you can search the weather table
            // using the location set by the user, which is only in the Location table.
            // So the convenience is worth it.
            WeatherContract.LocationEntry.TABLE_NAME + "." + WeatherContract.LocationEntry._ID,
            WeatherContract.LocationEntry.COLUMN_LOC_NAME,
            WeatherContract.LocationEntry.COLUMN_LOC_MUNICIPALITY,
            WeatherContract.LocationEntry.COLUMN_LOC_PROVINCE,
            WeatherContract.LocationEntry.COLUMN_LOC_TYPE,
            WeatherContract.LocationEntry.COLUMN_COORD_LONG,
            WeatherContract.LocationEntry.COLUMN_COORD_LAT

    };

    // Istos indices estan asociados a LOCALIDADE_COLUMNS.Se LOCALIDADE_COLUMNS cambia, os indices
    // deben ser cambiados.
    static final int COL_LOC_ID  = 0;
    static final int COL_LOC_NAME = 1;
    static final int COL_LOC_MUNICIPALITY = 2;
    static final int COL_LOC_PROVINCE = 3;
    static final int COL_LOC_TYPE = 4;
    static final int COL_COORD_LONG = 5;
    static final int COL_COORD_LAT = 6;

    private ForecastAdapter mLocationAdapter;

    public ForecastFragment() {
        // Require un constructor publico baleiro
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO a espera do avance na aplicacion, para cargar no inicio descomentar??
        //updateLocations();
        //para que este fragmento poda manexar eventos de menu
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Expande o menu; engade items ao action bar si esta presente.
        inflater.inflate(R.menu.forecastfragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            updateLocations();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // The CursorAdapter will take data from our cursor and populate the ListView.
        mLocationAdapter = new ForecastAdapter(getActivity(), null, 0);
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        //Consigue unha referencia do ListView existente lo 'layout' e asocialle o adaptador.
        ListView listView = (ListView) rootView.findViewById(R.id.listview_forecast);
        listView.setAdapter(mLocationAdapter);

        //Chamamos a noso MainActivity
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //O CursorAdapter devolta un cursor ca posicion correcta de getItem() ou null
            // se non pode atopar a posicion
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Cursor cursor = (Cursor) adapterView.getItemAtPosition(position);
                if (cursor != null) {
                    String locationSetting = Utility.getPreferredLocation(getActivity());
                    String localidade_id = String.valueOf(cursor.getInt(COL_LOC_ID));
                    Intent intent = new Intent(getActivity(), DetailActivity.class)
                            .setData(WeatherContract.LocationEntry.buildNameLocationId(locationSetting, localidade_id)
                            );
                    startActivity(intent);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstance){
        getLoaderManager().initLoader(LOCALIDADE_LOADER, null, this);
        super.onActivityCreated(savedInstance);
    }

    // since we read the location when we create the loader, all we need to do is restart things
    void onLocationChanged( ) {
        updateLocations();
        getLoaderManager().restartLoader(LOCALIDADE_LOADER, null, this);
    }

    private void updateLocations() {
        FetchWeatherTask weatherTask = new FetchWeatherTask(getActivity());
        String location = Utility.getPreferredLocation(getActivity());
        weatherTask.execute(location);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle){
        String locationSetting = Utility.getPreferredLocation(getActivity());
        String sortOrder = WeatherContract.LocationEntry.COLUMN_LOC_PROVINCE + " ASC";
        Uri localidadesForLocationUri = WeatherContract.LocationEntry.buildLocationSetting(
                locationSetting);
        return new CursorLoader(
                getActivity(),
                localidadesForLocationUri,
                LOCALIDADE_COLUMNS,
                null,
                null,
                sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor){
        mLocationAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader){
        mLocationAdapter.swapCursor(null);
    }
;

}
