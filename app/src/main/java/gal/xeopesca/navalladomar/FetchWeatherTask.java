/*
 * NavallaDoMar
 * Copyright (C) 2016  Jose Luis Villaverde Balsa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 3 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gal.xeopesca.navalladomar;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import gal.xeopesca.navalladomar.data.WeatherContract.LocationEntry;

public class FetchWeatherTask extends AsyncTask<String, Void, Void>{

    private final String LOG_TAG = FetchWeatherTask.class.getSimpleName();

    private final Context mContext;

    public FetchWeatherTask(Context context) {
        mContext = context;
    }

    private boolean DEBUG = true;

    //desde ForecastFragment executouse weatherTask.execute(location);
    @Override
    protected Void doInBackground(String... params) {

        // Se non existe localidade non hai nada que buscar. Verificamos o tamanho de params.
        if (params.length == 0) {
            return null;
        }
        String locationQuery = params[0];

        // Estos obxectos necesitan ser declarados fora do 'try/catch'
        // enton os podemos fechar no 'finally block'.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Conten a resposta JSON en bruto coma un 'string'.
        String forecastJsonStr = null;
        String format = "json";

        try {
            // construimos a URL para a consulta aos servizos de MeteoGalicia
            // Posibles parametros indicados na documentacion da API, en
            // http://www.meteogalicia.es/datosred/infoweb/meteo/proxectos/meteosix/API_MeteoSIX_gl.pdf
            // http://servizos.meteogalicia.es/api_manual/es/index.html
            final String FORECAST_BASE_URL =
                    "http://servizos.meteogalicia.es/apiv3/findPlaces?";
            final String QUERY_PARAM = "location";
            final String LANG_PARAM = "lang";
            final String FORMAT_PARAM = "format";
            final String API_KEY_PARAM = "API_KEY";

            Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                    .appendQueryParameter(QUERY_PARAM, params[0])
                    .appendQueryParameter(LANG_PARAM, "gl")
                    .appendQueryParameter(FORMAT_PARAM, "application/json")
                    .appendQueryParameter(API_KEY_PARAM, BuildConfig.OPEN_WEATHER_MAP_API_KEY)
                    .build();

            URL url = new URL(builtUri.toString());

            // Creaamos a peticion para servizos.meteogalicia.es, e abrimos a conexion
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            forecastJsonStr = buffer.toString();
        //TODO innecesario locationQuery
            //getWeatherDataFromJson(forecastJsonStr, locationQuery);
            getWeatherDataFromJson(forecastJsonStr);

        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            // If the location didn't successfully get the locations data, there's no point in attemping
            // to parse it.
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }
        //So ocurrira si existe un erro obtendo ou parseando a localidade (forecast)
        return null;
    }

    /**
     * Recibe o String que conten todas las localidades en formato JSON ao completo
     * e extrae os datos necesarios para construir os Strings necesarios para mostrar
     * (wireframes).
     *
     * O parseo e doado:  o constructor recibe o string JSON se o convirte en un obxecto
     * da xerarquia para nos.
     */
    //TODO innecesario locationSetting
    //private void getWeatherDataFromJson(String forecastJsonStr,String locationSetting)
    private void getWeatherDataFromJson(String forecastJsonStr)
            throws JSONException {

        // Estos son os nomes dos obxectos JSON que necesitamos extraer.
        final String OWM_FEATURES = "features";
        final String OWM_GEOMETRY = "geometry";
        final String OWM_COORDINATES = "coordinates";
        final String OWM_PROPERTIES = "properties";
        final String OWM_ID = "id";
        final String OWM_NAME = "name";
        final String OWM_MUNICIPALITY = "municipality";
        final String OWM_PROVINCE = "province";
        final String OWM_TYPE = "type";

        try {
            JSONObject forecastJson = new JSONObject(forecastJsonStr);
            JSONArray locationArray = forecastJson.getJSONArray(OWM_FEATURES);


            // Insert the new weather information into the database
            //Vector<ContentValues> cVVector = new Vector<ContentValues>(locationArray.length());

            int inserted = 0;
            for(int i = 0; i < locationArray.length(); i++) {
                // Valores a recoller
                String coordinates;
                String lonxitude;
                String latitude;
                String idLocalidade;
                String name;
                String municipality;
                String province;
                String type;

                JSONObject localizacion = locationArray.getJSONObject(i);

                JSONObject geometryObject = localizacion.getJSONObject(OWM_GEOMETRY);
                coordinates = geometryObject.getString(OWM_COORDINATES);
                String[] partCoordinates = coordinates.split(",");
                lonxitude = partCoordinates[0].substring(1).trim();
                latitude = partCoordinates[1].substring(0,partCoordinates[1].length()-1).trim();

                JSONObject propertiesObject = localizacion.getJSONObject(OWM_PROPERTIES);
                idLocalidade = propertiesObject.getString(OWM_ID);
                name = propertiesObject.getString(OWM_NAME);
                municipality = propertiesObject.getString(OWM_MUNICIPALITY);
                province = propertiesObject.getString(OWM_PROVINCE);
                type = propertiesObject.getString(OWM_TYPE);

                Integer locId = Integer.parseInt(idLocalidade);
                Double lonx = Double.parseDouble(lonxitude);
                Double lat = Double.parseDouble(latitude);
                //TODO para que?
                //temos o id do index, necesario mais adiante
                // engadimos a informacion a base de datos
                long id = this.addLocation(locId, name, municipality, province,type, lonx, lat);
                inserted +=1;
                //TODO pendente bulkInsert (peta polo idkey por non ser taboa relacionada)
                /*ContentValues locationValues = new ContentValues();
                locationValues.put(LocationEntry._ID, locId);
                locationValues.put(LocationEntry.COLUMN_LOC_NAME, name);
                locationValues.put(LocationEntry.COLUMN_LOC_MUNICIPALITY, municipality);
                locationValues.put(LocationEntry.COLUMN_LOC_PROVINCE, province);
                locationValues.put(LocationEntry.COLUMN_LOC_TYPE, type);
                locationValues.put(LocationEntry.COLUMN_COORD_LAT, lat);
                locationValues.put(LocationEntry.COLUMN_COORD_LONG, lonx);
                cVVector.add(locationValues);*/
            }
            // add to database
            /*if ( cVVector.size() > 0 ) {
                ContentValues[] cvArray = new ContentValues[cVVector.size()];
                cVVector.toArray(cvArray);
                inserted = mContext.getContentResolver().bulkInsert(LocationEntry.CONTENT_URI, cvArray);
            }*/
            Log.d(LOG_TAG, "FetchWeatherTask Complete. " + inserted + " Inserted");
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
    }

    /**
     * Helper method to handle insertion of a new location in the weather database.
     *
     * @param localidaIdeId O id da localidade (devolto na consulta web)
     * @param name O nome da localidade
     * @param latitude a latitude da localidade
     * @param lonxitude a longitude da localidade
     * @return o 'row ID' da localidade engadida.
     */
    long addLocation(Integer localidaIdeId, String name, String municipality,
                     String province,String type, double lonxitude,double latitude ) {

        long id;
        // Chequeamos se a localidade existe en db
        Cursor locationCursor = mContext.getContentResolver().query(
                LocationEntry.CONTENT_URI,
                new String[]{LocationEntry._ID},
                LocationEntry._ID + " = ?",
                new String[]{localidaIdeId.toString()},
                null);
        // Si existe, return o ID do rexistro en bd
        if (locationCursor.moveToFirst()){
            int locationIndex = locationCursor.getColumnIndex(LocationEntry._ID);
            id = locationCursor.getLong(locationIndex);
        }else{
            // Now that the content provider is set up, inserting rows of data is pretty simple.
            // First create a ContentValues object to hold the data you want to insert.
            ContentValues locationValues = new ContentValues();
            // Then add the data, along with the corresponding name of the data type,
            // so the content provider knows what kind of value is being inserted.
            locationValues.put(LocationEntry._ID, localidaIdeId);
            locationValues.put(LocationEntry.COLUMN_LOC_NAME, name);
            locationValues.put(LocationEntry.COLUMN_LOC_MUNICIPALITY, municipality);
            locationValues.put(LocationEntry.COLUMN_LOC_PROVINCE, province);
            locationValues.put(LocationEntry.COLUMN_LOC_TYPE, type);
            locationValues.put(LocationEntry.COLUMN_COORD_LAT, latitude);
            locationValues.put(LocationEntry.COLUMN_COORD_LONG, lonxitude);
            // Finally, insert location data into the database.
            Uri inserteUri = mContext.getContentResolver().insert(LocationEntry.CONTENT_URI,locationValues);
            // The resulting URI contains the ID for the row.  Extract the locationId from the Uri.
            id = ContentUris.parseId(inserteUri);
        }
        locationCursor.close();
        return id;
    }
}