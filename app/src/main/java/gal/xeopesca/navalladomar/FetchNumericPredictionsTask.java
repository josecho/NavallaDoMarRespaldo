package gal.xeopesca.navalladomar;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

import gal.xeopesca.navalladomar.data.WeatherContract;

/**
 * Created by tx on 4/1/16.
 */
public class FetchNumericPredictionsTask  extends AsyncTask<String, Void, Void> {

    private final String LOG_TAG = FetchWeatherTask.class.getSimpleName();

    private final Context mContext;

    public FetchNumericPredictionsTask(Context context) {
        mContext = context;
    }

    private boolean DEBUG = true;

    //desde FetchWeatherTask executouse FetchNumericPredictionsTask.execute(location);
    @Override
    protected Void doInBackground(String... params) {

        // Se non existe localidade non hai nada que buscar. Verificamos o tamanho de params.
        if (params.length == 0) {
            return null;
        }

        //TODO necesario?
        String idLocationQuery = params[0];

        // Estos obxectos necesitan ser declarados fora do 'try/catch'
        // entidon os podemos fechar no 'finally block'.
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Conten a resposta JSON en bruto coma un 'string'.
        String forecastJsonStr = null;
        String format = "json";

        try {
            // construimos a URL para a consulta aos servizos de MeteoGalicia
            // Posibles parametros indicados na documentacion da API, en
            // http://www.meteogalicia.es/datosred/infoweb/meteo/proxectos/meteosix/API_MeteoSIX_gl.pdf
            // http://servizos.meteogalicia.es/api_manual/es/index.html
            final String FORECAST_BASE_URL =
                    "http://servizos.meteogalicia.es/apiv3/getNumericForecastInfo?";
            final String QUERY_PARAM = "locationIds";
            //final String LANG_PARAM = "lang";
            //final String FORMAT_PARAM = "format";
            final String API_KEY_PARAM = "API_KEY";

            Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                    .appendQueryParameter(QUERY_PARAM, params[0])
                    //.appendQueryParameter(LANG_PARAM, "gl")
                    //appendQueryParameter(FORMAT_PARAM, "application/json")
                    .appendQueryParameter(API_KEY_PARAM, BuildConfig.OPEN_WEATHER_MAP_API_KEY)
                    .build();

            URL url = new URL(builtUri.toString());

            // Creaamos a peticion para servizos.meteogalicia.es, e abrimos a conexion
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            forecastJsonStr = buffer.toString();
            //TODO innecesario locationQuery
            //getWeatherDataFromJson(forecastJsonStr, locationQuery);
            getNumericPredictionsDataFromJson(forecastJsonStr);

        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            // If the location didn't successfully get the locations data, there's no point in attemping
            // to parse it.
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }
        //So ocurrira si existe un erro obtendo ou parseando a localidade (forecast)
        return null;
    }

    /**
     * Recibe o String que conten todas las localidades en formato JSON ao completo
     * e extrae os datos necesarios para construir os Strings necesarios para mostrar
     * (wireframes).
     *
     * O parseo e doado:  o constructor recibe o string JSON se o convirte en un obxecto
     * da xerarquia para nos.
     */
    //TODO innecesario locationSetting
    //private void getWeatherDataFromJson(String forecastJsonStr,String locationSetting)
    private void getNumericPredictionsDataFromJson(String forecastJsonStr)
            throws JSONException {

        // Estos son os nomes dos obxectos JSON que necesitamos extraer.
        final String OWM_FEATURES = "features";
        final String OWM_PROPERTIES = "properties";

        final String OWM_ID = "id";
        final String OWM_DAYS = "days";

        final String OWM_TIMEPERIOD = "timePeriod";
        final String OWM_BEGIN = "begin";
        final String OWM_END = "end";
        final String OWM_TIMEINSTANT = "timeInstant";

        final String OWM_VARIABLES = "variables";
        final String OWM_VALUES = "values";
        final String OWM_SKY_STATE = "value";

        try {
            //Location
            Long idLocalidade;
            //NumericFC
            Long numIdLocalidade;
            String dataNumericFc;
            //DaysEntry
            Long numericFCId;
            String timePeriodBegin;
            String timePeriodEnd;

            JSONObject forecastJson = new JSONObject(forecastJsonStr);
            JSONArray featuresNumericFCArray = forecastJson.getJSONArray(OWM_FEATURES);
            //O array features so conten un obxecto
            JSONObject featuresObjectZero = featuresNumericFCArray.getJSONObject(0);
            //Dentro de properties temos a informacion
            JSONObject propertiesObject = featuresObjectZero.getJSONObject(OWM_PROPERTIES);
            // O idLocalidade esta no primer nivel
            idLocalidade = propertiesObject.getLong(OWM_ID);
            // son os mesmos valores, fk e key
            numIdLocalidade=idLocalidade;
            // Extraemos o array dos dias
            JSONArray jsonDaysArray = propertiesObject.getJSONArray(OWM_DAYS);
            //Creamos se non existe a prediccion numerica
            Long idNumericFc = addNumericPrediction(idLocalidade);
            //ao refrescar necesitamos eliminar prediccions antiguas para actualizar
            //TODO en principio deberia eliminar todo en cascada, comprobar mais adiante
            mContext.getContentResolver().delete(WeatherContract.DaysEntry.CONTENT_URI,
                    WeatherContract.DaysEntry.COLUMN_DAYS_ID_NUMERICFC + "= ?",
                    new String[] { Long.toString(idNumericFc)});
            Vector<ContentValues> cVNewDays = new Vector<ContentValues>(jsonDaysArray.length());
            // Percorremos os dias
            for(int i = 0; i < jsonDaysArray.length(); i++) {
                JSONObject timePeriodAndVariables = jsonDaysArray.getJSONObject(i);
                timePeriodBegin =timePeriodAndVariables.getJSONObject(OWM_TIMEPERIOD).
                        getJSONObject(OWM_BEGIN).getString(OWM_TIMEINSTANT);
                timePeriodEnd =timePeriodAndVariables.getJSONObject(OWM_TIMEPERIOD).
                        getJSONObject(OWM_END).getString(OWM_TIMEINSTANT);
                ContentValues daysValues = new ContentValues();
                daysValues.put(WeatherContract.DaysEntry.COLUMN_DAYS_ID_NUMERICFC, idNumericFc);
                daysValues.put(WeatherContract.DaysEntry.COLUMN_DAYS_BEGIN, timePeriodBegin);
                daysValues.put(WeatherContract.DaysEntry.COLUMN_DAYS_END, timePeriodEnd);
                cVNewDays.add(daysValues);
            }
            int insertedNewDays = 0;
            // add to database
            if ( cVNewDays.size() > 0 ) {
                ContentValues[] cvArray = new ContentValues[cVNewDays.size()];
                cVNewDays.toArray(cvArray);
                insertedNewDays = mContext.getContentResolver().bulkInsert(
                        WeatherContract.DaysEntry.CONTENT_URI, cvArray);
            }
            Log.d(LOG_TAG, "FetchNumericPredictionsTask Complete. " + insertedNewDays + " Inserted");
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
    }

    /**
     * Metodo de axuda para manexar a insercion das prediccions numericas na base de datos weather.
     * @param localidadeId O id da localidade
     * @return o 'row ID' da prediccion numerica engadida.
     */
    long addNumericPrediction(Long localidadeId) {

        long id;
        // Chequeamos se a prediccion existe en db
        Cursor locationCursor = mContext.getContentResolver().query(
                WeatherContract.NumericFcEntry.CONTENT_URI,
                new String[]{WeatherContract.NumericFcEntry._ID},
                WeatherContract.NumericFcEntry.COLUMN_NUM_ID_LOC + " = ?",
                new String[]{localidadeId.toString()},
                null);
        // Si existe, return o ID do rexistro en bd
        if (locationCursor.moveToFirst()){
            int locationIndex = locationCursor.getColumnIndex(WeatherContract.NumericFcEntry._ID);
            id = locationCursor.getLong(locationIndex);
        }else{
            // Now that the content provider is set up, inserting rows of data is pretty simple.
            // First create a ContentValues object to hold the data you want to insert.
            ContentValues locationValues = new ContentValues();
            // Then add the data, along with the corresponding name of the data type,
            // so the content provider knows what kind of value is being inserted.
            locationValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_ID_LOC, localidadeId);
            //locationValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_DAY, day.toString());
            Uri inserteUri = mContext.getContentResolver().insert(
                    WeatherContract.NumericFcEntry.CONTENT_URI,locationValues);
            // The resulting URI contains the ID for the row.  Extract the locationId from the Uri.
            id = ContentUris.parseId(inserteUri);
        }
        locationCursor.close();
        return id;
    }
}
