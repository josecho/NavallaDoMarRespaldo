/*
 * NavallaDoMar
 * Copyright (C) 2016  Jose Luis Villaverde Balsa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 3 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gal.xeopesca.navalladomar;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import gal.xeopesca.navalladomar.data.WeatherContract;

/**
 * Created by tx on 2/7/16.
 */
public class DetailActivity extends AppCompatActivity {

    private final String LOG_TAG = DetailActivity.class.getSimpleName();
    private String textoDoDetalle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new DetailFragment())
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(this,SettingsActivity.class));
            return true;
        }
        if (id == R.id.action_map) {
            openPreferredLocationInMap();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openPreferredLocationInMap() {
        String localidade = getTextLocalidadeDetail();
        String[] coordinates = getCoordinates(localidade);
        String lonxitude = coordinates[0];
        String latitude = coordinates[1];
        // Using the URI scheme for showing a location found on a map.  This super-handy
        // intent can is detailed in the "Common Intents" page of Android's developer site:
        // http://developer.android.com/guide/components/intents-common.html#Maps
        String geoUri = String.format("geo:0,0?q=%s,%s(%s)", latitude, lonxitude, localidade);
        Uri geoLocation = Uri.parse(geoUri);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Log.d(LOG_TAG, "Couldn't call " + latitude + "," + lonxitude + ", no receiving apps installed!");
        }
    }

    /*
        Extrae os datos da localidade da vista de detalle
     */
    private String getTextLocalidadeDetail() {
        //accedemos a vista que mostra o detalle da localidade
        LayoutInflater inflater = getLayoutInflater();
        View myView = inflater.inflate(R.layout.activity_detail, null);
        TextView detalleLocalidadeView = (TextView) findViewById(R.id.detail_text);
        CharSequence textoDoDetalle = detalleLocalidadeView.getText();
        return charSequenceToString(textoDoDetalle);

    }

    private String charSequenceToString(CharSequence textoDoDetalle) {
        final StringBuilder sb = new StringBuilder(textoDoDetalle.length());
        sb.append(textoDoDetalle);
        return sb.toString();
    }

    private String[] getCoordinates(String localidade) {
        String[] camposLocalidade = localidade.split(":");
        return camposLocalidade[camposLocalidade.length - 1].split(",");
    }

    /**
     * Un fragment contendo unha simple vista.
     */
    public static class DetailFragment extends Fragment implements LoaderCallbacks<Cursor>{

        private static final String LOG_TAG = DetailFragment.class.getSimpleName();
        private static final String NAVALLA_SHARE_HASHTAG = "  #NavallaDoMarApp";
        private ShareActionProvider mShareActionProvider;
        private String mLocation;

        private static final int DETAIL_LOADER = 0;

        private static final String[] FORECAST_COLUMNS = {
                WeatherContract.LocationEntry.TABLE_NAME + "." + WeatherContract.LocationEntry._ID,
                WeatherContract.LocationEntry.COLUMN_LOC_NAME,
                WeatherContract.LocationEntry.COLUMN_LOC_MUNICIPALITY,
                WeatherContract.LocationEntry.COLUMN_LOC_PROVINCE,
                WeatherContract.LocationEntry.COLUMN_LOC_TYPE,
                WeatherContract.LocationEntry.COLUMN_COORD_LONG,
                WeatherContract.LocationEntry.COLUMN_COORD_LAT
        };

        //Estas constantes corresponden a proxeccionn definida encima, deben cambiar se a proxeccion
        //cambia
        static final int COL_LOC_ID  = 0;
        static final int COL_LOC_NAME = 1;
        static final int COL_LOC_MUNICIPALITY = 2;
        static final int COL_LOC_PROVINCE = 3;
        static final int COL_LOC_TYPE = 4;
        static final int COL_COORD_LONG = 5;
        static final int COL_COORD_LAT = 6;

        public DetailFragment() {
            setHasOptionsMenu(true);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_detail, container, false);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            // Inflate the menu; this adds items to the action bar if it is present.
            inflater.inflate(R.menu.detailfragment, menu);

            // Retrieve the share menu item
            MenuItem menuItem = menu.findItem(R.id.action_share);

            // Get the provider and hold onto it to set/change the share intent.
            mShareActionProvider =
                    (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);

            // If onLoadFinished happens before this, we can go ahead and set the share intent now.
            if (mLocation != null){
                mShareActionProvider.setShareIntent(createShareForecastIntent());
            }
        }

        private Intent createShareForecastIntent(){
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
            //deprecated change by NEW_DOCUMENT
            //shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, mLocation + NAVALLA_SHARE_HASHTAG);
            return shareIntent;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            getLoaderManager().initLoader(DETAIL_LOADER, null, this);
            super.onActivityCreated(savedInstanceState);
        }

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            Log.v(LOG_TAG, "In onCreateLoader");
            Intent intent = getActivity().getIntent();
            if (intent == null) {
                return null;
            }
            Uri localidadeForLocationNameAndIdUri = intent.getData();
            return new CursorLoader(
                    getActivity(),
                    localidadeForLocationNameAndIdUri,
                    FORECAST_COLUMNS,
                    null,
                    null,
                    null
            );
        }

        //TODO pide declarar Cursor final para acceder desde listView.setOnItemClickListener(
        @Override
        public void onLoadFinished(Loader<Cursor> loader, final Cursor data) {
            Log.v(LOG_TAG, "In onLoadFinished");
            if (!data.moveToFirst()) { return; }

            // no metodo onCreateView cargamos o layaout R.layout.fragment_detail
            // desde aqui accedemos aos elementos

            //carga e mostra localidade
            //String idLocalidade = String.valueOf(data.getInt(COL_LOC_ID));
            String name = data.getString(COL_LOC_NAME);
            String municipaly = data.getString(COL_LOC_MUNICIPALITY);
            String provincia = data.getString(COL_LOC_PROVINCE);
            String type = data.getString(COL_LOC_TYPE);
            String lonxitude = String.valueOf(data.getDouble(COL_COORD_LONG));
            String latitude = String.valueOf(data.getDouble(COL_COORD_LAT));
            //mLocation = String.format("%s : %s : %s : %s : %s : %s , %s", idLocalidade, name, municipaly,
                    //provincia,type ,lonxitude, latitude);
            mLocation = String.format("%s : %s : %s , %s", name, municipaly,
                    provincia,type);
            TextView detailTextView = (TextView)getView().findViewById(R.id.detail_text);
            detailTextView.setText(mLocation);

            //TODO ponto partida
            //cargamos datos asociados a localidade
            //updatePrediccions();

            //cargamos listado para escolla de prediccions
            //neste caso seria posible pasar o idLocalidade para busqueda
            String prediccionsNumericas = "Prediccions numericas: " + lonxitude + "," + latitude;
            String mareas = "Mareas: " + lonxitude + "," + latitude;
            String horarioSolar = "Horario solar: " + lonxitude + "," + latitude;
            //cargamos listado para escolla de prediccions
            ListView listView = (ListView) getView().findViewById(R.id.list_prediccions);
            String[] values = new String[] { prediccionsNumericas,mareas,horarioSolar};
            final ArrayList<String> list = new ArrayList<String>();
            for (int i = 0; i < values.length; ++i) {
                list.add(values[i]);
            }
            listView.setAdapter(new ArrayAdapter<String>(
                    getActivity().getApplicationContext(), R.layout.simpleprediccion, list));

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                //O CursorAdapter devolta un cursor ca posicion correcta de getItem() ou null
                // se non pode atopar a posicion
                // TODO O estou extraendo de ForecastFragment que chama a DetailActivity
                // TODO eu debo crear un novo activity
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    //Cursor cursor = (Cursor) adapterView.getItemAtPosition(position);
                    String seleccion = (String)adapterView.getItemAtPosition(position);
                    String firsLetter = seleccion.substring(0,1);
                    switch (firsLetter) {
                        case "P":
                            // todo
                            String locationSetting = Utility.getPreferredLocation(getActivity());
                            String localidade_id = String.valueOf(data.getInt(COL_LOC_ID));
                           /* Intent intent = new Intent(getActivity(), NumericPredictionsActivity.class)
                                    .setData(WeatherContract.LocationEntry.buildNameLocationId(locationSetting, localidade_id)
                                    );*/
                            Intent intent = new Intent(getActivity(), NumericPredictionsActivity.class)
                                    .putExtra("localidade_id", localidade_id);
                            startActivity(intent);

                            break;
                        case "M":
                            // todo
                            //String locationSetting = Utility.getPreferredLocation(getActivity());
                            //String localidade_id = String.valueOf(cursor.getInt(COL_LOC_ID));
                            /*intent = new Intent(getActivity(), DetailActivity.class)
                                    .setData(WeatherContract.LocationEntry.buildNameLocationId(locationSetting, localidade_id)
                                    );
                            startActivity(intent);*/
                            break;
                        case "H":
                            // todo
                            //String locationSetting = Utility.getPreferredLocation(getActivity());
                            //String localidade_id = String.valueOf(cursor.getInt(COL_LOC_ID));
                           /* intent = new Intent(getActivity(), DetailActivity.class)
                                    .setData(WeatherContract.LocationEntry.buildNameLocationId(locationSetting, localidade_id)
                                    );
                            startActivity(intent);*/
                            break;
                        default:
                            // todo
                            break;
                    }

                }
            });

            // If onCreateOptionsMenu has already happened, we need to update the share intent now.
            if (mShareActionProvider != null) {
                mShareActionProvider.setShareIntent(createShareForecastIntent());
            }

        }

        /*private void updatePrediccions() {
            FetchPrediccionsTask prediccionsTask = new FetchPrediccionsTask(getActivity());
            prediccionsTask.execute(mLocation);
        }*/

        @Override
        public void onLoaderReset(Loader<Cursor> loader) { }
    }

}
