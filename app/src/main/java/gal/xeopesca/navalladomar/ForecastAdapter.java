/*
 * NavallaDoMar
 * Copyright (C) 2016  Jose Luis Villaverde Balsa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 3 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gal.xeopesca.navalladomar;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * {@link ForecastAdapter} mostra unha lista das localidades
 * do {@link android.database.Cursor} ao {@link android.widget.ListView}.
 */
public class ForecastAdapter extends CursorAdapter{

    public ForecastAdapter(Context context, Cursor c, int flags){
        super(context, c, flags);
    }

    /*
        Remember that these views are reused as needed.
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_forecast, parent, false);
        return view;
    }

    /*
        This is where we fill-in the views with the contents of the cursor.
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        // our view is pretty simple here --- just a text view
        // we'll keep the UI functional with a simple (and slow!) binding.
        TextView tv = (TextView)view;
        tv.setText(convertCursorRowToUXFormat(cursor));
    }

    /*
        This is ported from FetchWeatherTask --- but now we go straight from the cursor to the
        string.
     */
    String convertCursorRowToUXFormat(Cursor cursor) {
        return  cursor.getString(ForecastFragment.COL_LOC_ID) +
                " - " + cursor.getString(ForecastFragment.COL_LOC_NAME)+
                " - " + cursor.getString(ForecastFragment.COL_LOC_MUNICIPALITY)+
                " - " + cursor.getString(ForecastFragment.COL_LOC_PROVINCE)+
                " - " + cursor.getString(ForecastFragment.COL_LOC_TYPE)+
                "- coordenadas:" +
                //primeiro lonxitude para que funcione no mapa
                cursor.getString(ForecastFragment.COL_COORD_LONG)+
                "," + cursor.getString(ForecastFragment.COL_COORD_LAT);
    }
}
