package gal.xeopesca.navalladomar;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;

import gal.xeopesca.navalladomar.data.WeatherContract;
import gal.xeopesca.navalladomar.data.WeatherDbHelper;

/**
 * Created by tx on 4/2/16.
 */
public class TestFetchNumericPredictionsTask extends AndroidTestCase {

    private static final String LOG_TAG = TestFetchNumericPredictionsTask.class.getSimpleName();

    public static final Long ADD_LOC_ID = Long.valueOf(1000658);
    //todo pendente si ou non data
    //public static final Date ADD_NUM_DAY = Date.valueOf( "2016-04-04" );

    /*public static final String ADD_NUM_SKY_STATE = "SUNNY";
    public static final Integer ADD_NUM_TEMPERATURE = 22;
    public static final Double ADD_NUM_PRECIPITATION = 3.33;
    public static final String ADD_NUM_WIND =  "3.44 6.77";
    public static final Double ADD_NUM_RELATIVE_HUMIDITY = 5.66 ;
    public static final Double ADD_NUM_CLOUD_AREA_FRACTION = 3.44;
    public static final Integer ADD_NUM_AIR_PREASSURE_AT_SEA_LEVEL = 4;
    public static final Integer ADD_NUM_SNOW_LEVEL = 6;
    public static final Integer ADD_NUM_SEA_WATER_TEMPERTATURE = 3;
    public static final Double ADD_NUM_SIGNIFICATIVE_WAVE_HEIGHT = 2.33;
    public static final Double ADD_NUM_MEAN_WAVE_DIRECTION = 1.13;
    public static final Integer ADD_NUM_RELATIVE_PEAK_PERIOD = 3;
    public static final Double ADD_NUM_SEA_WATER_SALINITY = 3.55;*/

    /*
        This test will only run on API level 11 and higher because of a requirement in the
        content provider.
     */
    @TargetApi(11)
    public void testAddLocation() {
        // start from a clean state
        if (true){
            int r = 3;
        }

        // elimina en cascada
        getContext().getContentResolver().delete(WeatherContract.LocationEntry.CONTENT_URI,
                WeatherContract.LocationEntry._ID + " = ?",
                new String[]{ADD_LOC_ID.toString()});

        FetchNumericPredictionsTask fnpt = new FetchNumericPredictionsTask(getContext());

        long idLocation = insertLocation();
        assertFalse("Error: insertLocation returned an invalid ID on insert",
                idLocation == -1);
        long indiceBd = fnpt.addNumericPrediction(idLocation);
        // does addLocation return a valid record ID?
        assertFalse("Error: addLocation returned an invalid ID on insert",
                indiceBd == -1);

        // test all this twice
        for ( int i = 0; i < 2; i++ ) {
            // does the ID point to our location?
            Cursor numericPredictionCursor = getContext().getContentResolver().query(
                    WeatherContract.NumericFcEntry.CONTENT_URI,
                    new String[]{
                            WeatherContract.NumericFcEntry._ID,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_ID_LOC,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_DAY

                            /*WeatherContract.NumericFcEntry.COLUMN_NUM_SKY_STATE,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_TEMPERATURE,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_PRECIPITATION,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_WIND,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_RELATIVE_HUMIDITY,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_CLOUD_AREA_FRACTION,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_AIR_PREASSURE_AT_SEA_LEVEL,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_SNOW_LEVEL,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_SEA_WATER_TEMPERTATURE,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_SIGNIFICATIVE_WAVE_HEIGHT,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_MEAN_WAVE_DIRECTION,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_RELATIVE_PEAK_PERIOD,
                            WeatherContract.NumericFcEntry.COLUMN_NUM_SEA_WATER_SALINITY,*/
                    },
                    WeatherContract.NumericFcEntry.COLUMN_NUM_ID_LOC + " = ?",
                    new String[]{ADD_LOC_ID.toString()},
                    null);

            // these match the indices of the projection
            if (numericPredictionCursor.moveToFirst()) {
                assertEquals("Error: the queried value of locationId does not match the returned value" +
                        "from addLocation", numericPredictionCursor.getLong(0), indiceBd);
                /*assertEquals("Error: the queried value of idlocation setting is incorrect",
                        locationCursor.getString(0), ADD_LOCATION_ID.toString());*/
                assertEquals("Error: the queried value of ADD_LOC_ID is incorrect",
                        numericPredictionCursor.getInt(1), ADD_LOC_ID.intValue());

                long datemillis = numericPredictionCursor.getLong(
                        numericPredictionCursor.getColumnIndex("day"));
                //String dateString = SimpleDateFormat("yyyy-MM-dd").format(new Date(datemillis));

                //todo pendente si ou non data
               /* assertEquals("Error: the queried value of ADD_NUM_SKY_STATE is incorrect",
                        numericPredictionCursor.getString(2), ADD_NUM_DAY.toString());*/

               /* assertEquals("Error: the queried value of ADD_NUM_SKY_STATE is incorrect",
                        numericPredictionCursor.getString(2), ADD_NUM_SKY_STATE);
                assertEquals("Error: the queried value of ADD_NUM_TEMPERATURE is incorrect",
                        numericPredictionCursor.getInt(3), ADD_NUM_TEMPERATURE.intValue());
                assertEquals("Error: the queried value of location type is incorrect",
                        numericPredictionCursor.getDouble(4), ADD_NUM_PRECIPITATION);
                assertEquals("Error: the queried value of ADD_NUM_WIND is incorrect",
                        numericPredictionCursor.getString(5), ADD_NUM_WIND);
                assertEquals("Error: the queried value of ADD_NUM_RELATIVE_HUMIDITY is incorrect",
                        numericPredictionCursor.getDouble(6), ADD_NUM_RELATIVE_HUMIDITY);
                assertEquals("Error: the queried value of ADD_NUM_RELATIVE_HUMIDITY is incorrect",
                        numericPredictionCursor.getDouble(7), ADD_NUM_CLOUD_AREA_FRACTION);
                assertEquals("Error: the queried value of ADD_NUM_RELATIVE_HUMIDITY is incorrect",
                        numericPredictionCursor.getInt(8), ADD_NUM_AIR_PREASSURE_AT_SEA_LEVEL.intValue());
                assertEquals("Error: the queried value of ADD_NUM_RELATIVE_HUMIDITY is incorrect",
                        numericPredictionCursor.getInt(9), ADD_NUM_SNOW_LEVEL.intValue());
                assertEquals("Error: the queried value of ADD_NUM_TEMPERATURE is incorrect",
                        numericPredictionCursor.getInt(10), ADD_NUM_SEA_WATER_TEMPERTATURE.intValue());
                assertEquals("Error: the queried value of ADD_NUM_RELATIVE_HUMIDITY is incorrect",
                        numericPredictionCursor.getDouble(11), ADD_NUM_SIGNIFICATIVE_WAVE_HEIGHT);
                assertEquals("Error: the queried value of ADD_NUM_RELATIVE_HUMIDITY is incorrect",
                        numericPredictionCursor.getDouble(12), ADD_NUM_MEAN_WAVE_DIRECTION);
                assertEquals("Error: the queried value of ADD_NUM_RELATIVE_PEAK_PERIOD is incorrect",
                        numericPredictionCursor.getInt(13), ADD_NUM_RELATIVE_PEAK_PERIOD.intValue());
                assertEquals("Error: the queried value of ADD_NUM_RELATIVE_HUMIDITY is incorrect",
                        numericPredictionCursor.getDouble(14), ADD_NUM_SEA_WATER_SALINITY);*/
            } else {
                fail("Error: the id you used to query returned an empty cursor");
            }

            // there should be no more records
            assertFalse("Error: there should be only one record returned from a location query",
                    numericPredictionCursor.moveToNext());

            // add the location again
            /*long newLocationId = fnpt.addNumericPrediction(ADD_LOC_ID, ADD_NUM_SKY_STATE, ADD_NUM_TEMPERATURE,
                    ADD_NUM_PRECIPITATION, ADD_NUM_WIND, ADD_NUM_RELATIVE_HUMIDITY, ADD_NUM_CLOUD_AREA_FRACTION,
                    ADD_NUM_AIR_PREASSURE_AT_SEA_LEVEL,ADD_NUM_SNOW_LEVEL,ADD_NUM_SEA_WATER_TEMPERTATURE,
                    ADD_NUM_SIGNIFICATIVE_WAVE_HEIGHT,ADD_NUM_MEAN_WAVE_DIRECTION,ADD_NUM_RELATIVE_PEAK_PERIOD,
                    ADD_NUM_SEA_WATER_SALINITY);*/

            //todo pendente si ou non data
            //long newLocationId = fnpt.addNumericPrediction(ADD_LOC_ID, ADD_NUM_DAY);
            long newNumericFCId = fnpt.addNumericPrediction(ADD_LOC_ID);

            assertEquals("Error: inserting a location again should return the same ID",
                    indiceBd, newNumericFCId);
        }
        // reset our state back to normal
        getContext().getContentResolver().delete(WeatherContract.NumericFcEntry.CONTENT_URI,
                WeatherContract.NumericFcEntry.COLUMN_NUM_ID_LOC + " = ?",
                new String[]{ADD_LOC_ID.toString()});
        // clean up the test so that other tests can use the content provider
        getContext().getContentResolver().
                acquireContentProviderClient(WeatherContract.NumericFcEntry.CONTENT_URI).
                getLocalContentProvider().shutdown();
    }

    public long insertLocation() {
        WeatherDbHelper dbHelper = new WeatherDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues testValues = new ContentValues();
        testValues.put(WeatherContract.LocationEntry._ID, ADD_LOC_ID);
        testValues.put(WeatherContract.LocationEntry.COLUMN_LOC_NAME, "Aguieira");
        testValues.put(WeatherContract.LocationEntry.COLUMN_LOC_MUNICIPALITY, "OLEIROS");
        testValues.put(WeatherContract.LocationEntry.COLUMN_LOC_PROVINCE, "A Coruña");
        testValues.put(WeatherContract.LocationEntry.COLUMN_LOC_TYPE, "locality");
        testValues.put(WeatherContract.LocationEntry.COLUMN_COORD_LAT, -8.3444);
        testValues.put(WeatherContract.LocationEntry.COLUMN_COORD_LONG, 43.35686);

        long locationRowId = db.insert(WeatherContract.LocationEntry.TABLE_NAME, null, testValues);
        // Verificamos que foi devolto o Id de columna.
        assertTrue(locationRowId != -1);
        db.close();

        return locationRowId;
    }



}
