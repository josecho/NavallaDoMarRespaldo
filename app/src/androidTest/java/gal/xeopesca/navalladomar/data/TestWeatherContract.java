/*
 * NavallaDoMar
 * Copyright (C) 2016  Jose Luis Villaverde Balsa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 3 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gal.xeopesca.navalladomar.data;

import android.net.Uri;
import android.test.AndroidTestCase;

import gal.xeopesca.navalladomar.data.WeatherContract.LocationEntry;


public class TestWeatherContract extends AndroidTestCase {

    // intentionally includes a slash to make sure Uri is getting quoted correctly
    private static final String TEST_LOCATION = "/Aguieira";
    private static final String TEST_LOCATION_ID = "/48136";
    private static final String TEST_NUMERICFC_ID = "/22";

    /*
        Test your LocationEntry.buildNameLocation function.
     */
    public void testBuildNameLocation() {
        Uri locationUri = LocationEntry.buildLocationSetting(TEST_LOCATION);
        assertNotNull("Error: Null Uri returned.  You must fill-in buildWeatherLocation in " +
                        "WeatherContract.",locationUri);
        assertEquals("Error: Weather location not properly appended to the end of the Uri",
                TEST_LOCATION, locationUri.getLastPathSegment());
        assertEquals("Error: Weather location Uri doesn't match our expected result",
                locationUri.toString(),
                "content://gal.xeopesca.navalladomar/location/%2FAguieira");
    }

    /*
        Test your LocationEntry.buildNameLocationId function.
     */
    public void testBuildNameLocationId() {
        Uri locationUri = LocationEntry.buildNameLocationId(TEST_LOCATION, TEST_LOCATION_ID);
        assertNotNull("Error: Null Uri returned.  You must fill-in buildWeatherLocation in " +
                "WeatherContract.",locationUri);
        assertEquals("Error: Weather location not properly appended to the end of the Uri",
                TEST_LOCATION_ID, locationUri.getLastPathSegment());
        assertEquals("Error: Weather location Uri doesn't match our expected result",
                locationUri.toString(),
                "content://gal.xeopesca.navalladomar/location/%2FAguieira/%2F48136");
    }

    /*
       Test your NumericFcEntry.builNumericPredictionLocation function.
    */
    public void testBuildNumericFcIdLocation() {
        Uri numericFcUri = WeatherContract.NumericFcEntry.builNumericPredictionwithIdLocation(TEST_LOCATION_ID);
        assertNotNull("Error: Null Uri returned.  You must fill-in builNumericPredictionLocation in " +
                "WeatherContract.",numericFcUri);
        assertEquals("Error: NumericFc, idLocation not properly appended to the end of the Uri",
                TEST_LOCATION_ID, numericFcUri.getLastPathSegment());
        assertEquals("Error: NumericFc idLocation Uri doesn't match our expected result",
                numericFcUri.toString(),
                "content://gal.xeopesca.navalladomar/getNumericForecastInfo/%2F48136");
    }

    /*
       Test your NumericFcEntry.builNumericPredictionLocation function.
    */
    public void testBuildDayswithIdNumericFc() {
        Uri dayswithIdNumericFcUri = WeatherContract.DaysEntry.builDayswithIdNumericFc(TEST_NUMERICFC_ID);
        assertNotNull("Error: Null Uri returned.  You must fill-in builDayswithIdNumericFc in " +
                "WeatherContract.",dayswithIdNumericFcUri);
        assertEquals("Error: Days, IdNumericFc not properly appended to the end of the Uri",
                TEST_NUMERICFC_ID, dayswithIdNumericFcUri.getLastPathSegment());
        assertEquals("Error: Days IdNumericFc Uri doesn't match our expected result",
                dayswithIdNumericFcUri.toString(),
                "content://gal.xeopesca.navalladomar/days/%2F22");
    }
}
