/*
 * NavallaDoMar
 * Copyright (C) 2016  Jose Luis Villaverde Balsa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 3 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gal.xeopesca.navalladomar.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;

import java.util.HashSet;

public class TestDb extends AndroidTestCase {

    public static final String LOG_TAG = TestDb.class.getSimpleName();

    /*
        Esta funcion e chamada antes da execucion de cada test para borrara a base de datos.
        Deste xeito sempre temos un test limpo.
     */
    public void setUp() {
        deleteTheDatabase();
    }

    //Cada test inicia cun estado limpo
    void deleteTheDatabase() {
        mContext.deleteDatabase(WeatherDbHelper.DATABASE_NAME);
    }

    public void testCreateDb() throws Throwable {
        // almacenamos nun HashSet todos os nomes data taboas a testear
        // A maiores existira outra taboa na DB que almacenara
        // os metadatos Android (informacion da version DB)
        final HashSet<String> tableNameHashSet = new HashSet<String>();
        tableNameHashSet.add(WeatherContract.LocationEntry.TABLE_NAME);
        tableNameHashSet.add(WeatherContract.NumericFcEntry.TABLE_NAME);
        tableNameHashSet.add(WeatherContract.DaysEntry.TABLE_NAME);
        tableNameHashSet.add(WeatherContract.SkyStateEntry.TABLE_NAME);

        mContext.deleteDatabase(WeatherDbHelper.DATABASE_NAME);
        SQLiteDatabase db = new WeatherDbHelper(
                this.mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());

        // Creamos as taboas que desexamos ?
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        assertTrue("Erro: Isto quere decir que a base de datos non foi creada correctamente",
                c.moveToFirst());

        // verificamos que as taboas foron creadas
        do {
           tableNameHashSet.remove(c.getString(0));
        } while( c.moveToNext() );

        // si erra, quere decir que a base de datos non conten a entrada da taboa location
        assertTrue("Error: Your database was created without both the location entry and weather entry tables",
                tableNameHashSet.isEmpty());

        // agora, contenhen as taboas as columnas correctas?
        c = db.rawQuery("PRAGMA table_info(" + WeatherContract.LocationEntry.TABLE_NAME + ")",
                null);

        assertTrue("Error: This means that we were unable to query the database for table information.",
                c.moveToFirst());

        // Construimos un HasSet de todos os nomes de columnas que queremos buscar
        final HashSet<String> locationColumnHashSet = new HashSet<String>();
        locationColumnHashSet.add(WeatherContract.LocationEntry._ID);
        locationColumnHashSet.add(WeatherContract.LocationEntry.COLUMN_LOC_NAME);
        locationColumnHashSet.add(WeatherContract.LocationEntry.COLUMN_LOC_MUNICIPALITY);
        locationColumnHashSet.add(WeatherContract.LocationEntry.COLUMN_LOC_PROVINCE);
        locationColumnHashSet.add(WeatherContract.LocationEntry.COLUMN_LOC_TYPE);
        locationColumnHashSet.add(WeatherContract.LocationEntry.COLUMN_COORD_LAT);
        locationColumnHashSet.add(WeatherContract.LocationEntry.COLUMN_COORD_LONG);

        int columnNameIndex = c.getColumnIndex("name");
        do {
            String columnName = c.getString(columnNameIndex);
            locationColumnHashSet.remove(columnName);
        } while(c.moveToNext());

        // se falla, quere decir que a base de datos non conten todas as columnas requeridas
        // de location
        assertTrue("Error: The database doesn't contain all of the required location entry columns",
                locationColumnHashSet.isEmpty());


        //NumericFcEntry
        c = db.rawQuery("PRAGMA table_info(" + WeatherContract.NumericFcEntry.TABLE_NAME + ")",
                null);

        assertTrue("Error: This means that we were unable to query the database for table information.",
                c.moveToFirst());

        // Construimos un HasSet de todos os nomes de columnas que queremos buscar
        final HashSet<String> numericFcColumnHashSet = new HashSet<String>();
        numericFcColumnHashSet.add(WeatherContract.NumericFcEntry._ID);
        numericFcColumnHashSet.add(WeatherContract.NumericFcEntry.COLUMN_NUM_ID_LOC);
        numericFcColumnHashSet.add(WeatherContract.NumericFcEntry.COLUMN_NUM_DAY);

        columnNameIndex = c.getColumnIndex("name");
        do {
            String columnName = c.getString(columnNameIndex);
            numericFcColumnHashSet.remove(columnName);
        } while(c.moveToNext());

        // se falla, quere decir que a base de datos non conten todas as columnas requeridas
        // de location
        assertTrue("Error: The database doesn't contain all of the required numericFc entry columns",
                numericFcColumnHashSet.isEmpty());


        //DaysEntry
        c = db.rawQuery("PRAGMA table_info(" + WeatherContract.DaysEntry.TABLE_NAME + ")",
                null);

        assertTrue("Error: This means that we were unable to query the database for table information.",
                c.moveToFirst());

        // Construimos un HasSet de todos os nomes de columnas que queremos buscar
        final HashSet<String> daysColumnHashSet = new HashSet<String>();
        daysColumnHashSet.add(WeatherContract.DaysEntry._ID);
        daysColumnHashSet.add(WeatherContract.DaysEntry.COLUMN_DAYS_ID_NUMERICFC);
        daysColumnHashSet.add(WeatherContract.DaysEntry.COLUMN_DAYS_BEGIN);
        daysColumnHashSet.add(WeatherContract.DaysEntry.COLUMN_DAYS_END);

        columnNameIndex = c.getColumnIndex("name");
        do {
            String columnName = c.getString(columnNameIndex);
            daysColumnHashSet.remove(columnName);
        } while(c.moveToNext());

        // se falla, quere decir que a base de datos non conten todas as columnas requeridas
        // de location
        assertTrue("Error: The database doesn't contain all of the required numericFc entry columns",
                daysColumnHashSet.isEmpty());

        //SkyStateEntry
        c = db.rawQuery("PRAGMA table_info(" + WeatherContract.SkyStateEntry.TABLE_NAME + ")",
                null);

        assertTrue("Error: This means that we were unable to query the database for table information.",
                c.moveToFirst());

        // Construimos un HasSet de todos os nomes de columnas que queremos buscar
        final HashSet<String> skyStateColumnHashSet = new HashSet<String>();
        skyStateColumnHashSet.add(WeatherContract.SkyStateEntry._ID);
        skyStateColumnHashSet.add(WeatherContract.SkyStateEntry.COLUMN_SKY_ID_DAY);
        skyStateColumnHashSet.add(WeatherContract.SkyStateEntry.COLUMN_SKY_STATE_VALUE);
        skyStateColumnHashSet.add(WeatherContract.SkyStateEntry.COLUMN_SKY_TIME_INSTANT);
        skyStateColumnHashSet.add(WeatherContract.SkyStateEntry.COLUMN_SKY_MODEL_RUN);
        skyStateColumnHashSet.add(WeatherContract.SkyStateEntry.COLUMN_SKY_ICON_URL);

        columnNameIndex = c.getColumnIndex("name");
        do {
            String columnName = c.getString(columnNameIndex);
            skyStateColumnHashSet.remove(columnName);
        } while(c.moveToNext());

        // se falla, quere decir que a base de datos non conten todas as columnas requeridas
        // de location
        assertTrue("Error: The database doesn't contain all of the required SkyState entry columns",
                skyStateColumnHashSet.isEmpty());
        db.close();
    }

    /*
        Testeamos os insert e query da base de datos.
        Os datos son creados e validados en TestUtilities
    */
    public void testLocationTable() {
        insertLocation();
    }

    /*
        Here is where you will build code to test that we can insert and query the
        database.You'll want to look in TestUtilities where you can use the "createNumericFcValues"
        function.You can also make use of the validateCurrentRecord function from within TestUtilities.
     */
    public void testNumericFcTable() {
        // First insert the location, and then use the locationRowId to insert
        // the numericFc. Make sure to cover as many failure cases as you can.

        // Instead of rewriting all of the code we've already written in testLocationTable
        // we can move this code to insertLocation and then call insertLocation from both
        // tests. Why move it? We need the code to return the ID of the inserted location
        // and our testLocationTable can only return void because it's a test.
        long locationRowId = insertLocation();

        // Make sure we have a valid row ID.
        assertFalse("Error: Location Not Inserted Correctly", locationRowId == -1L);

        // First step: Get reference to writable database
        // If there's an error in those massive SQL table creation Strings,
        // errors will be thrown here when you try to get a writable database.
        WeatherDbHelper dbHelper = new WeatherDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Second Step (Weather): Create weather values
        ContentValues numericFcValues = TestUtilities.createNumericFcValues(locationRowId);

        // Insert ContentValues into database and get a row ID back
        long numericFcRowId = db.insert(WeatherContract.NumericFcEntry.TABLE_NAME, null, numericFcValues);
        assertTrue(numericFcRowId != -1);

        // A cursor is your primary interface to the query results.
        Cursor numericFcCursor = db.query(
                WeatherContract.NumericFcEntry.TABLE_NAME,  // Table to Query
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null, // columns to group by
                null, // columns to filter by row groups
                null  // sort order
        );

        // Move the cursor to the first valid database row and check to see if we have any rows
        assertTrue("Error: No Records returned from numericFc query", numericFcCursor.moveToFirst());

        // Fifth Step: Validate the numericFc Query
        TestUtilities.validateCurrentRecord("testInsertReadDb NumericFcEntry failed to validate",
                numericFcCursor, numericFcValues);

        // Move the cursor to demonstrate that there is only one record in the database
        assertFalse( "Error: More than one record returned from NumericFc query",
                numericFcCursor.moveToNext() );

        //Inicio do test para days
        numericFcCursor.moveToFirst();
        //Estraemos o ID de numericFc para empregar como fk en Days, o introducimos en COLUMN_DAYS_ID_NUMERICFC
        Long idNumericFc = numericFcCursor.getLong(0);
        //Date day=Date.valueOf(stringday);
        ContentValues daysValues = TestUtilities.createDaysValues(idNumericFc);

        long daysRowId = db.insert(WeatherContract.DaysEntry.TABLE_NAME, null, daysValues);
        assertTrue(daysRowId != -1);

        // A cursor is your primary interface to the query results.
        Cursor daysCursor = db.query(
                WeatherContract.DaysEntry.TABLE_NAME,  // Table to Query
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null, // columns to group by
                null, // columns to filter by row groups
                null  // sort order
        );

        // Move the cursor to the first valid database row and check to see if we have any rows
        assertTrue("Error: No Records returned from Days query", daysCursor.moveToFirst());

        // Fifth Step: Validate the numericFc Query
        TestUtilities.validateCurrentRecord("testInsertReadDb DaysEntry failed to validate",
                daysCursor, daysValues);

        // Move the cursor to demonstrate that there is only one record in the database
        assertFalse( "Error: More than one record returned from Days query",
                daysCursor.moveToNext() );

        // Inicio do test para skyState
        daysCursor.moveToFirst();
        //Extraemos o id de dayCursor para empregar como fk en skyState, o introducimos en COLUMN_SKY_STATE_ID
        Long IdDay = daysCursor.getLong(0);
        ContentValues skyStateValues = TestUtilities.createSkySgtateValues(IdDay);
        long skyStateRowId = db.insert(WeatherContract.SkyStateEntry.TABLE_NAME, null, skyStateValues);
        assertTrue(skyStateRowId != -1);

        // A cursor is your primary interface to the query results.
        Cursor skyStateCursor = db.query(
                WeatherContract.SkyStateEntry.TABLE_NAME,  // Table to Query
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null, // columns to group by
                null, // columns to filter by row groups
                null  // sort order
        );

        // Move the cursor to the first valid database row and check to see if we have any rows
        assertTrue("Error: No Records returned from Days query", skyStateCursor.moveToFirst());

        // Fifth Step: Validate the numericFc Query
        TestUtilities.validateCurrentRecord("testInsertReadDb DaysEntry failed to validate",
                skyStateCursor, skyStateValues);

        // Move the cursor to demonstrate that there is only one record in the database
        assertFalse("Error: More than one record returned from Days query",
                skyStateCursor.moveToNext());

        // Sixth Step: Close cursor and database
        skyStateCursor.close();
        daysCursor.close();
        numericFcCursor.close();
        dbHelper.close();
    }

    /*
        This is a helper method. You can call this code from both
        testNumericTable and testLocationTable.
     */
    public long insertLocation() {
        // First step: Get reference to writable database
        // If there's an error in those massive SQL table creation Strings,
        // errors will be thrown here when you try to get a writable database.
        WeatherDbHelper dbHelper = new WeatherDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Creamos os ContentValues que queremos insertar
        ContentValues testValues = TestUtilities.createLocationValues();

        // Insertamos ContentValues na base de datos e obtemos de volta o ID da columna
        long locationRowId;
        locationRowId = db.insert(WeatherContract.LocationEntry.TABLE_NAME, null, testValues);

        // Verificamos que foi devolto o Id de columna.
        assertTrue(locationRowId != -1);
        // Data's inserted.  IN THEORY.  Now pull some out to stare at it and verify it made
        // the round trip.

        // Executamos unha Query na base de datos e obtemos un Cursor
        // Un cursor a interface primaria dos resultados da query.
        Cursor cursor = db.query(
                WeatherContract.LocationEntry.TABLE_NAME,  // Table to Query
                null, // all columns
                null, // Columns for the "where" clause
                null, // Values for the "where" clause
                null, // columns to group by
                null, // columns to filter by row groups
                null // sort order
        );

        // Movemos o cursor a unha columna valida da base de datos e chequeamos se foi devolto
        // algun rexistro pola query
        assertTrue("Erro: Non foron devoltos rexistros pola consulta(query) a location", cursor.moveToFirst());
        //validacion dos datos do resultado do Cursor cos ContenValues orixinais
        TestUtilities.validateCurrentRecord("Erro: Location Query Validation Failed",
                cursor, testValues);
        //movemos o cursor para demostrar que so existe un rexistro na base de datos
        assertFalse("Erro: Mais dun rexistro devolto pola consulta(query) a location", cursor.moveToNext());
        //fechamos o cursor e a base de datos.
        cursor.close();
        db.close();

        return locationRowId;
    }

}
