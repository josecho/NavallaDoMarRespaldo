/*
 * NavallaDoMar
 * Copyright (C) 2016  Jose Luis Villaverde Balsa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 3 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gal.xeopesca.navalladomar.data;

import android.content.ComponentName;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.test.AndroidTestCase;
import android.util.Log;

import gal.xeopesca.navalladomar.data.WeatherContract.DaysEntry;
import gal.xeopesca.navalladomar.data.WeatherContract.LocationEntry;
import gal.xeopesca.navalladomar.data.WeatherContract.NumericFcEntry;
import gal.xeopesca.navalladomar.data.WeatherContract.SkyStateEntry;
/*
    Non e un conxunto completo de tests do ContentProvider de NavallaDoMarMX, testea si as
    funcionalidades basicas foron implementadas correctamente
 */
public class TestProvider extends AndroidTestCase {

    public static final String LOG_TAG = TestProvider.class.getSimpleName();

    // Since we want each test to start with a clean slate, run deleteAllRecords
    // in setUp (called by the test runner before each test).
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        deleteAllRecords();
    }

    public void deleteAllRecords() {
        deleteAllRecordsFromProvider();
    }

    /*
       Esta funcion elimina todos os rexistros da taboa de base de datos usando o ContentProvider.
       Realiza unha consulta ao ContentProvider para estar seguros que os datos foron eliminados.
     */
    public void deleteAllRecordsFromProvider() {

        // DELETE IN CASCADE
        mContext.getContentResolver().delete(
                LocationEntry.CONTENT_URI,
                null,
                null
        );

        //QUERY
        Cursor cursor = mContext.getContentResolver().query(
                SkyStateEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        assertEquals("Error: Records not deleted from SkyStateEntry table during delete", 0, cursor.getCount());
        cursor.close();

        cursor = mContext.getContentResolver().query(
                DaysEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        assertEquals("Error: Records not deleted from DaysEntry table during delete", 0, cursor.getCount());
        cursor.close();

        cursor = mContext.getContentResolver().query(
                NumericFcEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        assertEquals("Error: Records not deleted from NumericFcEntry table during delete", 0, cursor.getCount());
        cursor.close();

        cursor = mContext.getContentResolver().query(
                LocationEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        assertEquals("Error: Records not deleted from Location table during delete", 0, cursor.getCount());
        cursor.close();

    }

    /*
        Chequea para asegurar que o "content provider" esta rexistrado correctamente.
     */
    public void testProviderRegistry() {
        PackageManager pm = mContext.getPackageManager();

        // Definimos o component name basandonos no nome do paquete do contexto e a clase
        // WeatherProvider
        ComponentName componentName = new ComponentName(mContext.getPackageName(),
                WeatherProvider.class.getName());
        try {
            // Procuramos o informacion do "provider" empregando o nome do componente desde
            // o paquete
            // Lanza unha excepcion se o "provider" non esta rexistrado
            ProviderInfo providerInfo = pm.getProviderInfo(componentName, 0);
            // Aseguramonos de que a autoridade rexistrada coincida ca autoridade do Contrato
            assertEquals("Error: WeatherProvider registered with authority: " + providerInfo.authority +
                    " instead of authority: " + WeatherContract.CONTENT_AUTHORITY,
                    providerInfo.authority, WeatherContract.CONTENT_AUTHORITY);
        } catch (PackageManager.NameNotFoundException e) {
            // I guess the provider isn't registered correctly.
            assertTrue("Error: WeatherProvider not registered at " + mContext.getPackageName(),
                    false);
        }
    }

    /*
     O test non toca a base de datos. Verifica que o ContentProvider devolve o tipo correcto
     por cada tipo de URI que pode manexar
     */
    public void testGetType() {
        // content://gal.xeopesca.navalladomar/location/
        String type = mContext.getContentResolver().getType(LocationEntry.CONTENT_URI);
        // vnd.android.cursor.dir/gal.xeopesca.navalladomar/weather
        assertEquals("Error: the LocationEntry CONTENT_URI should return LocationEntry.CONTENT_TYPE",
                LocationEntry.CONTENT_TYPE, type);

        String testLocation = "Aguieria";
        // content://gal.xeopesca.navalladomar/location/Aguieria
        type = mContext.getContentResolver().getType(
                LocationEntry.buildLocationSetting(testLocation));
        // vnd.android.cursor.dir/com.example.android.sunshine.app/weather
        assertEquals("Error: the LocationEntry CONTENT_URI with location should return LocationEntry.CONTENT_TYPE",
                LocationEntry.CONTENT_TYPE, type);

        String idLocation = "1000658";
        // content://gal.xeopesca.navalladomar/location/Aguieria/1000658
        type = mContext.getContentResolver().getType(
                LocationEntry.buildNameLocationId(testLocation, idLocation));
        // vnd.android.cursor.item/gal.xeopesca.navalladomar/weather
        assertEquals("Error: the LocationEntry CONTENT_URI with location and idLocation should return " +
                        "LocationEntry.CONTENT_ITEM_TYPE",
                LocationEntry.CONTENT_ITEM_TYPE, type);

        // content://gal.xeopesca.navalladomar/getNumericForecastInfo/
        type = mContext.getContentResolver().getType(NumericFcEntry.CONTENT_URI);
        // vnd.android.cursor.dir/gal.xeopesca.navalladomar/weather
        assertEquals("Error: the NumericFcEntry CONTENT_URI should return NumericFcEntry.CONTENT_TYPE",
                NumericFcEntry.CONTENT_TYPE, type);
        // content://gal.xeopesca.navalladomar/getNumericForecastInfo/1000658
        type = mContext.getContentResolver().getType(
                NumericFcEntry.builNumericPredictionwithIdLocation(idLocation));
        // vnd.android.cursor.item/gal.xeopesca.navalladomar/weather
        assertEquals("Error: the NumericFcEntry CONTENT_URI with idLocation should return " +
                        "NumericFcEntry.CONTENT_ITEM_TYPE",
                NumericFcEntry.CONTENT_ITEM_TYPE, type);

        String idNumericFc = "22";
        // content://gal.xeopesca.navalladomar/days/
        type = mContext.getContentResolver().getType(DaysEntry.CONTENT_URI);
        // vnd.android.cursor.dir/gal.xeopesca.navalladomar/weather
        assertEquals("Error: the LocationEntry CONTENT_URI should return DaysEntry.CONTENT_TYPE",
                DaysEntry.CONTENT_TYPE, type);
        //content://gal.xeopesca.navalladomar/days/22
        type = mContext.getContentResolver().getType(DaysEntry.builDayswithIdNumericFc(idNumericFc));
        // vnd.android.cursor.item/gal.xeopesca.navalladomar/weather
        assertEquals("Error: the DaysEntry CONTENT_URI with idNumericFc should return " +
                        "NumericFcEntry.CONTENT_ITEM_TYPE",
                DaysEntry.CONTENT_ITEM_TYPE, type);

        String idDay = "33";
        // content://gal.xeopesca.navalladomar/skystate/
        type = mContext.getContentResolver().getType(SkyStateEntry.CONTENT_URI);
        // vnd.android.cursor.dir/gal.xeopesca.navalladomar/weather
        assertEquals("Error: the LocationEntry CONTENT_URI should return DaysEntry.CONTENT_TYPE",
                SkyStateEntry.CONTENT_TYPE, type);
        //content://gal.xeopesca.navalladomar/skystate/8
        type = mContext.getContentResolver().getType(SkyStateEntry.builSkyStateWithIdDay(idDay));
        // vnd.android.cursor.item/gal.xeopesca.navalladomar/weather
        assertEquals("Error: the SkyStateEntry CONTENT_URI with idDay should return " +
                        "SkyStateEntry.CONTENT_ITEM_TYPE",
                SkyStateEntry.CONTENT_ITEM_TYPE, type);
    }

    /*
        This test uses the database directly to insert and then uses the ContentProvider to
        read out the data.
     */
    public void testBasicMXQuery() {
        // insert our test records into the database
        WeatherDbHelper dbHelper = new WeatherDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues testValues = TestUtilities.createLocationValues();
        long locationRowId = TestUtilities.insertLocationValues(mContext);

        // Now that we have a location, add some numericPredictions values!
        ContentValues numericPredictionsValues = TestUtilities.createNumericFcValues(locationRowId);

        long numericFcRowId = db.insert(NumericFcEntry.TABLE_NAME, null, numericPredictionsValues);
        assertTrue("Unable to Insert NumericFcEntry into the Database", numericFcRowId != -1);

        ContentValues daysValues = TestUtilities.createDaysValues(numericFcRowId);
        long IdDays = db.insert(DaysEntry.TABLE_NAME, null, daysValues);
        assertTrue("Unable to Insert DaysEntry into the Database", IdDays != 1);

        ContentValues skyStateValues = TestUtilities.createSkySgtateValues(IdDays);
        long skyStateRowId = db.insert(SkyStateEntry.TABLE_NAME, null, skyStateValues);
        assertTrue("Unable to Insert SkyStateEntry into the Database", skyStateRowId != 1);

        db.close();

        // Test the basic content provider query
        Cursor cursor = mContext.getContentResolver().query(
                NumericFcEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        // Make sure we get the correct cursor out of the database
        TestUtilities.validateCursor("testBasicMXQuery NumericFcEntry", cursor, numericPredictionsValues);

        cursor = mContext.getContentResolver().query(
                DaysEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        TestUtilities.validateCursor("testBasicMXQuery DaysEntry", cursor, daysValues);

        cursor = mContext.getContentResolver().query(
                SkyStateEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );
        TestUtilities.validateCursor("testBasicMXQuery DaysEntry", cursor, skyStateValues);

        cursor.close();
    }

    /*
        This test uses the database directly to insert and then uses the ContentProvider to
        read out the data.
     */
    public void testBasicLocationQueries() {
        // insert our test records into the database
        WeatherDbHelper dbHelper = new WeatherDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues testLocationValues = TestUtilities.createLocationValues();
        long locationRowId = TestUtilities.insertLocationValues(mContext);

        // Test the basic content provider query
        Cursor locationCursor = mContext.getContentResolver().query(
                LocationEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );

        // Make sure we get the correct cursor out of the database
        TestUtilities.validateCursor("testBasicLocationQueries, location query", locationCursor, testLocationValues);

        // Has the NotificationUri been set correctly? --- we can only test this easily against API
        // level 19 or greater because getNotificationUri was added in API level 19.
        if ( Build.VERSION.SDK_INT >= 19 ) {
            assertEquals("Error: Location Query did not properly set NotificationUri",
                    locationCursor.getNotificationUri(), LocationEntry.CONTENT_URI);
        }
    }

    /*
        O test emprega o provider para insertar e actualizar os datos
     */
    public void testUpdate() {
        //Creamos un novo mapa de valores onde os nomes das columnas son as chaves (keys)
        ContentValues testLocationValues = TestUtilities.createLocationValues();

        Uri locationUri = mContext.getContentResolver().
                insert(LocationEntry.CONTENT_URI, testLocationValues);
        // Converts the last path segment to a long
        long locationRowId = ContentUris.parseId(locationUri);

        // Verifica que devolveu un identidicador de columna.
        assertTrue(locationRowId != -1);
        Log.d(LOG_TAG, "New row id: " + locationRowId);

        ContentValues updatedLocationValues = new ContentValues(testLocationValues);
        //TODO dubido que sexa necesario, o _ID non se debe actualizar
        updatedLocationValues.put(LocationEntry._ID, locationRowId);
        updatedLocationValues.put(LocationEntry.COLUMN_LOC_NAME, "AguieriraRogelio");

        // Creamos un cursor con un observador para asegurarnos que o content provider notifica
        // as observacion que son esperadas
        Cursor locationCursor = mContext.getContentResolver().query(
                LocationEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
        TestUtilities.TestContentObserver tco = TestUtilities.getTestContentObserver();
        //Register an observer that is called when changes happen to the content backing this cursor
        locationCursor.registerContentObserver(tco);

        int count = mContext.getContentResolver().update(
                LocationEntry.CONTENT_URI, updatedLocationValues, LocationEntry._ID + "= ?",
                new String[] { Long.toString(locationRowId)});
        assertEquals(count, 1);

        // Test para asegurarnos de que noso observador e chamado. Se non, lanzase unha asercion.
        // Se o codigo erra aqui, quere decir que o content provider
        // non esta chamando  a  getContext().getContentResolver().notifyChange(uri, null);
        tco.waitForNotificationOrFail();
        locationCursor.unregisterContentObserver(tco);
        locationCursor.close();


        ContentValues testNumericFcValues = TestUtilities.createNumericFcValues(locationRowId);
        Uri numeriFcUri = mContext.getContentResolver().
                insert(NumericFcEntry.CONTENT_URI, testNumericFcValues);
        // Converts the last path segment to a long
        long numericFcRowId = ContentUris.parseId(numeriFcUri);
        // Verifica que devolveu un identidicador de columna.
        assertTrue(numericFcRowId != -1);
        Log.d(LOG_TAG, "New row id: " + numericFcRowId);
        ContentValues updatedNumericFcValues = new ContentValues(testNumericFcValues);
        updatedNumericFcValues.put(NumericFcEntry.COLUMN_NUM_ID_LOC, locationRowId);
        updatedNumericFcValues.put(NumericFcEntry.COLUMN_NUM_DAY,"2016-04-05" );
        // Creamos un cursor con un observador para asegurarnos que o content provider notifica
        // as observacion que son esperadas
        Cursor numericFcCursor = mContext.getContentResolver().query(
                NumericFcEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
        TestUtilities.TestContentObserver tcoN  = TestUtilities.getTestContentObserver();
        //Register an observer that is called when changes happen to the content backing this cursor
        numericFcCursor.registerContentObserver(tcoN);
        count = mContext.getContentResolver().update(
                NumericFcEntry.CONTENT_URI, updatedNumericFcValues, NumericFcEntry.COLUMN_NUM_ID_LOC + "= ?",
                new String[] { Long.toString(locationRowId)});
        assertEquals(count, 1);
        tcoN.waitForNotificationOrFail();
        numericFcCursor.unregisterContentObserver(tcoN);
        numericFcCursor.close();

        ContentValues testDaysValues = TestUtilities.createDaysValues(numericFcRowId);
        Uri daysFcUri = mContext.getContentResolver().
                insert(DaysEntry.CONTENT_URI, testDaysValues);
        // Converts the last path segment to a long
        long dayRowId = ContentUris.parseId(daysFcUri);
        // Verifica que devolveu un identidicador de columna.
        assertTrue(dayRowId != -1);
        Log.d(LOG_TAG, "New row id: " + dayRowId);
        ContentValues updatedDaysValues = new ContentValues(testDaysValues);
        updatedDaysValues.put(DaysEntry.COLUMN_DAYS_ID_NUMERICFC, numericFcRowId);
        updatedDaysValues.put(DaysEntry.COLUMN_DAYS_BEGIN,"2016-04-05T17:00:10+02" );
        updatedDaysValues.put(DaysEntry.COLUMN_DAYS_END,"2016-04-05T23:59:59+02" );
        // Creamos un cursor con un observador para asegurarnos que o content provider notifica
        // as observacion que son esperadas
        Cursor daysCursor = mContext.getContentResolver().query(
                DaysEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
        TestUtilities.TestContentObserver tcoD  = TestUtilities.getTestContentObserver();
        //Register an observer that is called when changes happen to the content backing this cursor
        daysCursor.registerContentObserver(tcoD);
        count = mContext.getContentResolver().update(
                DaysEntry.CONTENT_URI, updatedDaysValues, DaysEntry.COLUMN_DAYS_ID_NUMERICFC + "= ?",
                new String[] { Long.toString(numericFcRowId)});
        assertEquals(count, 1);
        tcoD.waitForNotificationOrFail();
        daysCursor.unregisterContentObserver(tcoD);
        daysCursor.close();

        ContentValues testSkyStateValues = TestUtilities.createSkySgtateValues(dayRowId);
        Uri skyStateUri = mContext.getContentResolver().
                insert(SkyStateEntry.CONTENT_URI, testSkyStateValues);
        // Converts the last path segment to a long
        long skyStateRowId = ContentUris.parseId(skyStateUri);
        // Verifica que devolveu un identidicador de columna.
        assertTrue(skyStateRowId != -1);
        Log.d(LOG_TAG, "New row id: " + skyStateRowId);
        ContentValues updatedSkyStateValues = new ContentValues(testSkyStateValues);
        updatedSkyStateValues.put(SkyStateEntry.COLUMN_SKY_ID_DAY, dayRowId);
        updatedSkyStateValues.put(SkyStateEntry.COLUMN_SKY_TIME_INSTANT,"2016-04-05T17:00:10+02" );
        updatedSkyStateValues.put(SkyStateEntry.COLUMN_SKY_MODEL_RUN,"2016-04-04T23:59:59+02" );
        updatedSkyStateValues.put(SkyStateEntry.COLUMN_SKY_ICON_URL,"http://gnu.com/image.png" );
        // Creamos un cursor con un observador para asegurarnos que o content provider notifica
        // as observacion que son esperadas
        Cursor SkyStateCursor = mContext.getContentResolver().query(
                SkyStateEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
        TestUtilities.TestContentObserver tcoSk  = TestUtilities.getTestContentObserver();
        //Register an observer that is called when changes happen to the content backing this cursor
        SkyStateCursor.registerContentObserver(tcoSk);
        count = mContext.getContentResolver().update(
                SkyStateEntry.CONTENT_URI, updatedSkyStateValues, SkyStateEntry.COLUMN_SKY_ID_DAY + "= ?",
                new String[] { Long.toString(dayRowId)});
        assertEquals(count, 1);
        tcoSk.waitForNotificationOrFail();
        SkyStateCursor.unregisterContentObserver(tcoSk);
        SkyStateCursor.close();

       //VALIDATE
       // A cursor is your primary interface to the query results.
        Cursor cursor = mContext.getContentResolver().query(
                LocationEntry.CONTENT_URI,
                null,   // projection
                LocationEntry._ID + " = " + locationRowId,
                null,   // Values for the "where" clause
                null    // sort order
        );
        TestUtilities.validateCursor("testUpdateLocation.  Error validating location entry update.",
                cursor, updatedLocationValues);

        cursor = mContext.getContentResolver().query(
                NumericFcEntry.CONTENT_URI,
                null,   // projection
                NumericFcEntry.COLUMN_NUM_ID_LOC + " = " + locationRowId,
                null,   // Values for the "where" clause
                null    // sort order
        );
        TestUtilities.validateCursor("testUpdateLocation.  Error validating location entry update.",
                cursor, updatedNumericFcValues);

        cursor = mContext.getContentResolver().query(
                DaysEntry.CONTENT_URI,
                null,   // projection
                DaysEntry.COLUMN_DAYS_ID_NUMERICFC + " = " + numericFcRowId,
                null,   // Values for the "where" clause
                null    // sort order
        );
        TestUtilities.validateCursor("testUpdateDays.  Error validating location entry update.",
                cursor, updatedDaysValues);

        cursor = mContext.getContentResolver().query(
                SkyStateEntry.CONTENT_URI,
                null,   // projection
                SkyStateEntry.COLUMN_SKY_ID_DAY + " = " + dayRowId,
                null,   // Values for the "where" clause
                null    // sort order
        );
        TestUtilities.validateCursor("testUpdateDays.  Error validating location entry update.",
                cursor, updatedSkyStateValues);

        cursor.close();

        //eliminamos se non queda gravado rexistro
        deleteAllRecordsFromProvider();
    }

    // Make sure we can still delete after adding/updating stuff
    public void testInsertReadProvider() {

        ContentValues testLocationValues = TestUtilities.createLocationValues();
        // Register a content observer for our insert.  This time, directly with the content resolver
        TestUtilities.TestContentObserver tco = TestUtilities.getTestContentObserver();
        mContext.getContentResolver().registerContentObserver(LocationEntry.CONTENT_URI, true, tco);
        Uri locationUri = mContext.getContentResolver().insert(LocationEntry.CONTENT_URI, testLocationValues);
        // Foi chamado noso content observer?
        // se falla, non esta chamando  a  getContext().getContentResolver().notifyChange(uri, null);
        tco.waitForNotificationOrFail();
        mContext.getContentResolver().unregisterContentObserver(tco);
        // Converts the last path segment to a long
        long locationRowId = ContentUris.parseId(locationUri);
        // Verify we got a row back.
        assertTrue(locationRowId != -1);
        // Data's inserted.  IN THEORY.  Now pull some out to stare at it and verify it made
        // the round trip.
        // A cursor is your primary interface to the query results.
        Cursor cursor = mContext.getContentResolver().query(
                LocationEntry.CONTENT_URI,
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );
        TestUtilities.validateCursor("testInsertReadProvider. Error validating LocationEntry.",
                cursor, testLocationValues);

        // Agora que temos unha localidade engadimos prediccions numericas
        ContentValues testNumericPredictionsValues = TestUtilities.createNumericFcValues(locationRowId);
        // The TestContentObserver is a one-shot class
        tco = TestUtilities.getTestContentObserver();
        mContext.getContentResolver().registerContentObserver(NumericFcEntry.CONTENT_URI, true, tco);
        Uri numericPredictionsInsertUri = mContext.getContentResolver()
                .insert(NumericFcEntry.CONTENT_URI, testNumericPredictionsValues);
        assertTrue(numericPredictionsInsertUri != null);
        // Esta sendo chamado o "content observer"?. Se erra, a inserccion da prediccion numerica
        // no ContentProvider non esta sendo chamada
        // getContext().getContentResolver().notifyChange(uri, null);
        tco.waitForNotificationOrFail();
        mContext.getContentResolver().unregisterContentObserver(tco);
        // A cursor is your primary interface to the query results.
        Cursor numericPredictionCursor = mContext.getContentResolver().query(
                NumericFcEntry.CONTENT_URI,  // Table to Query
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null // columns to group by
        );
        TestUtilities.validateCursor("testInsertReadProvider. Error validating NumericFcEntry insert.",
                numericPredictionCursor, testNumericPredictionsValues);

        // Add the location values in with the weather data so that we can make
        // sure that the join worked and we actually get all the values back
        // Get the joined Weather and Location data
        numericPredictionCursor = mContext.getContentResolver().query(
                NumericFcEntry.builNumericPredictionwithIdLocation(TestUtilities.TEST_ID_LOCATION),
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );
        ContentValues testNumericPredictionsAndLocationValues = testLocationValues;
        testNumericPredictionsAndLocationValues.putAll(testNumericPredictionsValues);
        TestUtilities.validateCursor("testInsertReadProvider.  Error validating joined numericPredictions and Location Data.",
                numericPredictionCursor, testNumericPredictionsAndLocationValues);

        // Agora que temos unha prediccion mumerica engadimos os dias
        long numericFcRowId = ContentUris.parseId(numericPredictionsInsertUri);
        // Verify we got a row back.
        assertTrue(numericFcRowId != -1);
        ContentValues testDaysValues = TestUtilities.createDaysValues(numericFcRowId);
        // The TestContentObserver is a one-shot class
        tco = TestUtilities.getTestContentObserver();
        mContext.getContentResolver().registerContentObserver(DaysEntry.CONTENT_URI, true, tco);
        Uri DaysInsertUri = mContext.getContentResolver()
                .insert(DaysEntry.CONTENT_URI, testDaysValues);
        assertTrue(DaysInsertUri != null);
        tco.waitForNotificationOrFail();
        mContext.getContentResolver().unregisterContentObserver(tco);
        // A cursor is your primary interface to the query results.
        Cursor daysCursor = mContext.getContentResolver().query(
                DaysEntry.builDayswithIdNumericFc(String.valueOf(numericFcRowId)),// Table to Query
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null // columns to group by
        );
        ContentValues testNumericPredictionsAndDaysValues = testNumericPredictionsValues;
        testNumericPredictionsAndDaysValues.putAll(testDaysValues);
        TestUtilities.validateCursor("testInsertReadProvider. Error validating NumericFcEntry insert.",
                daysCursor, testNumericPredictionsAndDaysValues);

        // Agora que temos un dia lle asociamos un skystate
        long dayRowId = ContentUris.parseId(DaysInsertUri);
        // Verify we got a row back.
        assertTrue(numericFcRowId != -1);
        ContentValues testSkyStateValues = TestUtilities.createSkySgtateValues(dayRowId);
        // The TestContentObserver is a one-shot class
        tco = TestUtilities.getTestContentObserver();
        mContext.getContentResolver().registerContentObserver(SkyStateEntry.CONTENT_URI, true, tco);
        Uri skyStateInsertUri = mContext.getContentResolver()
                .insert(SkyStateEntry.CONTENT_URI, testSkyStateValues);
        assertTrue(skyStateInsertUri != null);
        tco.waitForNotificationOrFail();
        mContext.getContentResolver().unregisterContentObserver(tco);
        // A cursor is your primary interface to the query results.
        Cursor skyStateCursor = mContext.getContentResolver().query(
                SkyStateEntry.builSkyStateWithIdDay(String.valueOf(dayRowId)),// Table to Query
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null // columns to group by
        );
        ContentValues testDaysAndSkyStateValues = testDaysValues;
        testDaysAndSkyStateValues.putAll(testSkyStateValues);
        TestUtilities.validateCursor("testInsertReadProvider. Error validating SkyStateEntry insert.",
                skyStateCursor, testDaysAndSkyStateValues);
    }

    // Aseguramonos de poder borrar despois de engadir ou actualizar algo
    // O metodo anterior testInsertReadProvider ten que executarse previamente sactisfactoriamente
    // as insercions e consultas deben ser completadas antes de executar este test
    public void testDeleteRecords() {

        testInsertReadProvider();

        // Rexistramos a content observer para o eliminado da nosa localidade
        TestUtilities.TestContentObserver locationObserver = TestUtilities.getTestContentObserver();
        mContext.getContentResolver().registerContentObserver(
                LocationEntry.CONTENT_URI, true, locationObserver);

        /*TestUtilities.TestContentObserver numericPredictionObserver = TestUtilities.getTestContentObserver();
        mContext.getContentResolver().registerContentObserver(
                NumericFcEntry.CONTENT_URI, true, numericPredictionObserver);

        TestUtilities.TestContentObserver daysObserver = TestUtilities.getTestContentObserver();
        mContext.getContentResolver().registerContentObserver(
                DaysEntry.CONTENT_URI, true, daysObserver);

       TestUtilities.TestContentObserver skyStateObserver = TestUtilities.getTestContentObserver();
        mContext.getContentResolver().registerContentObserver(
                SkyStateEntry.CONTENT_URI, true, skyStateObserver);*/

        deleteAllRecordsFromProvider();

        // If either of these fail, you most-likely are not calling the
        // getContext().getContentResolver().notifyChange(uri, null); in the ContentProvider delete.
        // (only if the insertReadProvider is succeeding)
        locationObserver.waitForNotificationOrFail();
        /*numericPredictionObserver.waitForNotificationOrFail();
        daysObserver.waitForNotificationOrFail();
        skyStateObserver.waitForNotificationOrFail();*/

        mContext.getContentResolver().unregisterContentObserver(locationObserver);
        /*mContext.getContentResolver().unregisterContentObserver(numericPredictionObserver);
        mContext.getContentResolver().unregisterContentObserver(daysObserver);
        mContext.getContentResolver().unregisterContentObserver(skyStateObserver);*/
    }

    static private final int BULK_INSERT_RECORDS_TO_INSERT = 10;
    // Note that this test will work with the built-in (default) provider
    // implementation, which just inserts records one-at-a-time, so really do implement the
    // BulkInsert ContentProvider function.
    //TODO pendente de ver como insertar os datos asociados a cada dia, na mesma taboa ou non
    public void testBulkInsertDays() {
        // Creamos unha localidade cos seus valores
        ContentValues testLocationValues = TestUtilities.createLocationValues();
        Uri locationUri = mContext.getContentResolver().insert(LocationEntry.CONTENT_URI, testLocationValues);
        // Converte o ultimo segmento do path nun long
        long locationRowId = ContentUris.parseId(locationUri);

        // Verify we got a row back.
        assertTrue(locationRowId != -1);

        // Data's inserted.  IN THEORY.  Now pull some out to stare at it and verify it made
        // the round trip.

        // A cursor is your primary interface to the query results.
        Cursor cursorLocation = mContext.getContentResolver().query(
                LocationEntry.CONTENT_URI,
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null  // sort order
        );

        assertEquals(cursorLocation.getCount(), 1);
        TestUtilities.validateCursor("testBulkInsert. Error validating LocationEntry.",
                cursorLocation, testLocationValues);

        //Creamos unha prediccion numerica
        ContentValues testNumericFcValues = TestUtilities.createNumericFcValues(locationRowId);
        Uri numericFcUri = mContext.getContentResolver().insert(NumericFcEntry.CONTENT_URI, testNumericFcValues);
        // Converte o ultimo segmento do path nun long
        long numericFcId = ContentUris.parseId(numericFcUri);
        // Verify we got a row back.
        assertTrue(numericFcId != -1);
        // Data's inserted.  IN THEORY.  Now pull some out to stare at it and verify it made
        // the round trip.
        Cursor cursorNumericFc = mContext.getContentResolver().query(
                NumericFcEntry.CONTENT_URI,
                null,
                null,
                null,
                null
        );

        assertEquals(cursorNumericFc.getCount(), 1);
        TestUtilities.validateCursor("testBulkInsert. Error validating NumericFcEntry.",
                cursorNumericFc, testNumericFcValues);

        // Now we can bulkInsert some days.
        // With ContentProviders, you really only have to implement the features you use, after all.
        ContentValues[] bulkInsertDaysContentValues = createBulkInsertDaysValues(numericFcId);

        // Register a content observer for our bulk insert.
        TestUtilities.TestContentObserver daysObserver = TestUtilities.getTestContentObserver();
        mContext.getContentResolver().registerContentObserver(
                DaysEntry.CONTENT_URI, true, daysObserver);

        int insertCount = mContext.getContentResolver().bulkInsert(
                DaysEntry.CONTENT_URI, bulkInsertDaysContentValues);

        // If this fails, it means that you most-likely are not calling the
        // getContext().getContentResolver().notifyChange(uri, null); in your BulkInsert
        // ContentProvider method.
        daysObserver.waitForNotificationOrFail();
        mContext.getContentResolver().unregisterContentObserver(daysObserver);

        assertEquals(insertCount, BULK_INSERT_RECORDS_TO_INSERT);

        // A cursor is your primary interface to the query results.
        Cursor cursorDays = mContext.getContentResolver().query(
                DaysEntry.CONTENT_URI,
                null, // leaving "columns" null just returns all the columns.
                null, // cols for "where" clause
                null, // values for "where" clause
                null
        );

        // we should have as many records in the database as we've inserted
        assertEquals(cursorDays.getCount(), BULK_INSERT_RECORDS_TO_INSERT);

        // and let's make sure they match the ones we created
        cursorDays.moveToFirst();
        for ( int i = 0; i < BULK_INSERT_RECORDS_TO_INSERT; i++, cursorDays.moveToNext() ) {
            TestUtilities.validateCurrentRecord("testBulkInsert.  Error validating daysEntry " + i,
                    cursorDays, bulkInsertDaysContentValues[i]);
        }
        cursorDays.close();
        cursorNumericFc.close();
        cursorLocation.close();
    }

    static ContentValues[] createBulkInsertDaysValues(long lnumericFcId) {
        ContentValues[] returnContentDaysValues = new ContentValues[BULK_INSERT_RECORDS_TO_INSERT];
        for ( int i = 0; i < BULK_INSERT_RECORDS_TO_INSERT; i++) {
            ContentValues daysValues = new ContentValues();
            daysValues.put(DaysEntry.COLUMN_DAYS_ID_NUMERICFC, lnumericFcId);
            daysValues.put(DaysEntry.COLUMN_DAYS_BEGIN, "2016-04-04T17:00:10+02");
            daysValues.put(DaysEntry.COLUMN_DAYS_END, "2016-04-04T23:59:59+02");
            returnContentDaysValues[i] = daysValues;
        }
        return returnContentDaysValues;
    }
}
