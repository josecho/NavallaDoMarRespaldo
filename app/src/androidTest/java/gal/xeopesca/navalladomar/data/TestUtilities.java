/*
 * NavallaDoMar
 * Copyright (C) 2016  Jose Luis Villaverde Balsa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 3 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gal.xeopesca.navalladomar.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.test.AndroidTestCase;

import java.util.Map;
import java.util.Set;

import gal.xeopesca.navalladomar.utils.PollingCheck;

public class TestUtilities extends AndroidTestCase {

    //TODO este valor e valido para Sunshine, aqui revisar
    static final String TEST_LOCATION = "aguie";
    static final String TEST_ID_LOCATION = "48136";

    static void validateCursor(String error, Cursor valueCursor, ContentValues expectedValues) {
        assertTrue("Empty cursor returned. " + error, valueCursor.moveToFirst());
        validateCurrentRecord(error, valueCursor, expectedValues);
        valueCursor.close();
    }

    static ContentValues createLocationValues() {
        //Creamos un novo mapa de valores onde os nomes das columnas son as chaves (keys)
        ContentValues testValues = new ContentValues();
        //identificador da localidade devolto na consulta a API de MeteoSix
        testValues.put(WeatherContract.LocationEntry._ID, 48136);
        testValues.put(WeatherContract.LocationEntry.COLUMN_LOC_NAME, "Aguieira");
        testValues.put(WeatherContract.LocationEntry.COLUMN_LOC_MUNICIPALITY, "OLEIROS");
        testValues.put(WeatherContract.LocationEntry.COLUMN_LOC_PROVINCE, "A Coruña");
        testValues.put(WeatherContract.LocationEntry.COLUMN_LOC_TYPE, "locality");
        testValues.put(WeatherContract.LocationEntry.COLUMN_COORD_LAT, -8.3444);
        testValues.put(WeatherContract.LocationEntry.COLUMN_COORD_LONG, 43.35686);
        return testValues;
    }

    /*
       Use this to create some default numericFc values for your database tests.
    */
    static ContentValues createNumericFcValues(long locationRowId) {
        ContentValues weatherValues = new ContentValues();
        weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_ID_LOC, locationRowId);
        //TODO pendente engadir campos timestamp

        /*weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_SKY_STATE, "SUNNY");
        weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_TEMPERATURE, 22);
        weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_PRECIPITATION,3.33 );
        weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_WIND,"3.44 6.77" );
        weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_RELATIVE_HUMIDITY,5.66 );
        weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_CLOUD_AREA_FRACTION,3.44 );
        weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_AIR_PREASSURE_AT_SEA_LEVEL,4);
        weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_SNOW_LEVEL, 6);
        weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_SEA_WATER_TEMPERTATURE,3 );
        weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_SIGNIFICATIVE_WAVE_HEIGHT,2.33);
        weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_MEAN_WAVE_DIRECTION,1.13 );
        weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_RELATIVE_PEAK_PERIOD,3 );
        weatherValues.put(WeatherContract.NumericFcEntry.COLUMN_NUM_SEA_WATER_SALINITY,3.55 );*/
        return weatherValues;
    }

    /*
       Use this to create some default days values for your database tests.
    */
    static ContentValues createDaysValues(Long idNumericFc) {
        ContentValues weatherValues = new ContentValues();
        //fk to NumericFc
        weatherValues.put(WeatherContract.DaysEntry.COLUMN_DAYS_ID_NUMERICFC,idNumericFc );
        weatherValues.put(WeatherContract.DaysEntry.COLUMN_DAYS_BEGIN, "2016-04-04T17:00:10+02");
        weatherValues.put(WeatherContract.DaysEntry.COLUMN_DAYS_END, "2016-04-04T23:59:59+02");
        return weatherValues;
    }

    /*
       Use this to create some default skystate values for your database tests.
    */
    static ContentValues createSkySgtateValues(Long IdDay) {
        ContentValues weatherValues = new ContentValues();
        //fk to days
        weatherValues.put(WeatherContract.SkyStateEntry.COLUMN_SKY_ID_DAY,IdDay );
        weatherValues.put(WeatherContract.SkyStateEntry.COLUMN_SKY_TIME_INSTANT, "2016-04-04T17:00:10+02");
        weatherValues.put(WeatherContract.SkyStateEntry.COLUMN_SKY_MODEL_RUN, "2016-04-04T23:59:59+02");
        weatherValues.put(WeatherContract.SkyStateEntry.COLUMN_SKY_ICON_URL, "http://url.com/image.png");
        return weatherValues;
    }

    static void validateCurrentRecord(String error, Cursor valueCursor, ContentValues expectedValues) {
        Set<Map.Entry<String, Object>> valueSet = expectedValues.valueSet();
        for (Map.Entry<String, Object> entry : valueSet) {
            String columnName = entry.getKey();
            int idx = valueCursor.getColumnIndex(columnName);
            assertFalse("Column '" + columnName + "' not found. " + error, idx == -1);
            String expectedValue = entry.getValue().toString();
            String temos = valueCursor.getString(idx);
            assertEquals(columnName + " Value '" + entry.getValue().toString() +
                    "' did not match the expected value '" +
                    expectedValue + "'. " + error, expectedValue, valueCursor.getString(idx));
        }
    }


    static long insertLocationValues(Context context) {
        // insert our test records into the database
        WeatherDbHelper dbHelper = new WeatherDbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues testValues = TestUtilities.createLocationValues();

        long locationRowId;
        locationRowId = db.insert(WeatherContract.LocationEntry.TABLE_NAME, null, testValues);

        // Verify we got a row back.
        assertTrue("Error: Failure to insert Aguieira Location Values", locationRowId != -1);

        return locationRowId;
    }

    /*
        The functions we provide inside of TestProvider use this utility class to test
        the ContentObserver callbacks using the PollingCheck class that we grabbed from the Android
        CTS tests.

        Note that this only tests that the onChange function is called; it does not test that the
        correct Uri is returned.
     */
    static class TestContentObserver extends ContentObserver {
        final HandlerThread mHT;
        boolean mContentChanged;

        static TestContentObserver getTestContentObserver() {
            HandlerThread ht = new HandlerThread("ContentObserverThread");
            ht.start();
            return new TestContentObserver(ht);
        }

        private TestContentObserver(HandlerThread ht) {
            super(new Handler(ht.getLooper()));
            mHT = ht;
        }

        // On earlier versions of Android, this onChange method is called
        @Override
        public void onChange(boolean selfChange) {
            onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            mContentChanged = true;
        }

        public void waitForNotificationOrFail() {
            // Note: The PollingCheck class is taken from the Android CTS (Compatibility Test Suite).
            // It's useful to look at the Android CTS source for ideas on how to test your Android
            // applications.  The reason that PollingCheck works is that, by default, the JUnit
            // testing framework is not running on the main Android application thread.
            new PollingCheck(5000) {
                @Override
                protected boolean check() {
                    return mContentChanged;
                }
            }.run();
            mHT.quit();
        }
    }

    static TestContentObserver getTestContentObserver() {
        return TestContentObserver.getTestContentObserver();
    }
}
