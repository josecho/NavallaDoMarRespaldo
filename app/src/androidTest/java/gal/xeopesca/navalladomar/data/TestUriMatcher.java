/*
 * NavallaDoMar
 * Copyright (C) 2016  Jose Luis Villaverde Balsa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 3 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gal.xeopesca.navalladomar.data;

import android.content.UriMatcher;
import android.net.Uri;
import android.test.AndroidTestCase;

/*
    Testa o UriMatcher. Esta clase emprega constantes que son declaradas con proteccion de
    paquete dentro do UriMatcher, polo cal, o test debe estar no mesmo paquete do codigo da aplicacion
    Android. Seguindo este camiño mantemos o compromiso entre a ocultacion de informacion e a testeabilidade.
 */
public class TestUriMatcher extends AndroidTestCase {

    private static final String LOCATION_QUERY = "Aguieria";
    //TODO revisar integer ou string
    private static final String TEST_LOCATION_ID = "1000345";
    private static final String TEST_NUMERIC_PREDICTION_ID = "22";

    // content://com.example.android.sunshine.app/location"
    private static final Uri TEST_LOCATION_DIR = WeatherContract.LocationEntry.CONTENT_URI;
    // content://com.example.android.sunshine.app/location/Aguieira"
    private static final Uri TEST_LOCATION_WITH_NAME_DIR = WeatherContract.LocationEntry.buildLocationSetting(LOCATION_QUERY);
    //  content://com.example.android.sunshine.app/location/Aguieira/1000345"
    private static final Uri TEST_LOCATION_WITH_NAME_AND_LOC_ID_DIR = WeatherContract.LocationEntry.
            buildNameLocationId(LOCATION_QUERY, TEST_LOCATION_ID);

    // content://com.example.android.sunshine.app/getNumericForecastInfo"
    private static final Uri TEST_NUMERIC_PREDICTION_DIR = WeatherContract.NumericFcEntry.CONTENT_URI;
    //  content://com.example.android.sunshine.app/getNumericForecastInfo/1000345"
    private static final Uri TEST_NUMERIC_PREDICTION_WITH_LOC_ID_DIR = WeatherContract.NumericFcEntry.
            builNumericPredictionwithIdLocation(TEST_LOCATION_ID);

    // content://com.example.android.sunshine.app/days"
    private static final Uri TEST_DAYS_DIR = WeatherContract.DaysEntry.CONTENT_URI;
    //  content://com.example.android.sunshine.app/days/22"
    private static final Uri TEST_DAYS_WITH_NUMERIC_PREDICTION_ID_DIR = WeatherContract.DaysEntry.
            builDayswithIdNumericFc(TEST_NUMERIC_PREDICTION_ID);

    /*
        Esta funcion testea que a UriMatcher retorne o valor enteiro correcto por cada tipo de Uri
        que noso ContentProvider manexa.
     */
    public void testUriMatcher() {
        UriMatcher testMatcher = WeatherProvider.buildUriMatcher();

        assertEquals("Error: The LOCATION URI was matched incorrectly.",
                testMatcher.match(TEST_LOCATION_DIR), WeatherProvider.LOCATION);
        assertEquals("Error: The LOCATION URI WITH NAME was matched incorrectly.",
                testMatcher.match(TEST_LOCATION_WITH_NAME_DIR), WeatherProvider.LOCATION_WITH_NAME);
        assertEquals("Error: The LOCATION URI WITH NAME AND LOC_ID was matched incorrectly.",
                testMatcher.match(TEST_LOCATION_WITH_NAME_AND_LOC_ID_DIR), WeatherProvider.LOCATION_WITH_NAME_AND_LOC_ID);

        assertEquals("Error: The NUMERIC_PREDICTION URI was matched incorrectly.",
                testMatcher.match(TEST_NUMERIC_PREDICTION_DIR), WeatherProvider.NUMERIC_PREDICTION);
        assertEquals("Error: The NUMERIC_PREDICTION URI WITH LOC_ID was matched incorrectly.",
                testMatcher.match(TEST_NUMERIC_PREDICTION_WITH_LOC_ID_DIR), WeatherProvider.NUMERIC_PREDICTION_WITH_LOC_ID);

        assertEquals("Error: The DAYS URI was matched incorrectly.",
                testMatcher.match(TEST_DAYS_DIR), WeatherProvider.DAYS);
        assertEquals("Error: The DAYS URI WITH NUMERIC_PREDICTION_ID was matched incorrectly.",
                testMatcher.match(TEST_DAYS_WITH_NUMERIC_PREDICTION_ID_DIR), WeatherProvider.DAYS_WITH_NUMERICFC_ID);

    }
}
