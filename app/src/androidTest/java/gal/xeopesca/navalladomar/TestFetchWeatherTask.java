/*
 * NavallaDoMar
 * Copyright (C) 2016  Jose Luis Villaverde Balsa
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation (version 3 of the License).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package gal.xeopesca.navalladomar;

import android.annotation.TargetApi;
import android.database.Cursor;
import android.test.AndroidTestCase;

import gal.xeopesca.navalladomar.data.WeatherContract;

public class TestFetchWeatherTask extends AndroidTestCase {

    static final Integer ADD_LOCATION_ID = 1000658;
    static final String ADD_LOCATION_NAME = "Praia Aguieria ou de Cedeira";
    static final String ADD_LOCATION_MUNICIPALITY = "Porto do Son";
    static final String ADD_LOCATION_PROVINCE = "A Coruña";
    static final String ADD_LOCATION_TYPE = "Beach";
    static final double ADD_LOCATION_LAT = 42.74167;
    static final double ADD_LOCATION_LON = -8.97018;

    /*
        This test will only run on API level 11 and higher because of a requirement in the
        content provider.
     */
    @TargetApi(11)
    public void testAddLocation() {
        // start from a clean state
        if (true){
            int r = 3;
        }

        getContext().getContentResolver().delete(WeatherContract.LocationEntry.CONTENT_URI,
                WeatherContract.LocationEntry._ID + " = ?",
                new String[]{ADD_LOCATION_ID.toString()});
        //CursorAdapter
        //FetchWeatherTask fwt = new FetchWeatherTask(getContext(), null);
        FetchWeatherTask fwt = new FetchWeatherTask(getContext());
        long indiceBd = fwt.addLocation(ADD_LOCATION_ID, ADD_LOCATION_NAME, ADD_LOCATION_MUNICIPALITY,
                ADD_LOCATION_PROVINCE, ADD_LOCATION_TYPE, ADD_LOCATION_LAT, ADD_LOCATION_LON);

        // does addLocation return a valid record ID?
        assertFalse("Error: addLocation returned an invalid ID on insert",
                indiceBd == -1);

        // test all this twice
        for ( int i = 0; i < 2; i++ ) {
            // does the ID point to our location?
            Cursor locationCursor = getContext().getContentResolver().query(
                    WeatherContract.LocationEntry.CONTENT_URI,
                    new String[]{
                            WeatherContract.LocationEntry._ID,
                            WeatherContract.LocationEntry.COLUMN_LOC_NAME,
                            WeatherContract.LocationEntry.COLUMN_LOC_MUNICIPALITY,
                            WeatherContract.LocationEntry.COLUMN_LOC_PROVINCE,
                            WeatherContract.LocationEntry.COLUMN_LOC_TYPE,
                            WeatherContract.LocationEntry.COLUMN_COORD_LAT,
                            WeatherContract.LocationEntry.COLUMN_COORD_LONG
                    },
                    WeatherContract.LocationEntry._ID + " = ?",
                    new String[]{ADD_LOCATION_ID.toString()},
                    null);

            // these match the indices of the projection
            if (locationCursor.moveToFirst()) {
                assertEquals("Error: the queried value of locationId does not match the returned value" +
                        "from addLocation", locationCursor.getLong(0), indiceBd);
                assertEquals("Error: the queried value of idlocation setting is incorrect",
                        locationCursor.getString(0), ADD_LOCATION_ID.toString());
                assertEquals("Error: the queried value of location name is incorrect",
                        locationCursor.getString(1), ADD_LOCATION_NAME);
                assertEquals("Error: the queried value of location municipality is incorrect",
                        locationCursor.getString(2), ADD_LOCATION_MUNICIPALITY);
                assertEquals("Error: the queried value of location province is incorrect",
                        locationCursor.getString(3), ADD_LOCATION_PROVINCE);
                assertEquals("Error: the queried value of location type is incorrect",
                        locationCursor.getString(4), ADD_LOCATION_TYPE);
                assertEquals("Error: the queried value of latitude is incorrect",
                        locationCursor.getDouble(5), ADD_LOCATION_LON);
                assertEquals("Error: the queried value of longitude is incorrect",
                        locationCursor.getDouble(6), ADD_LOCATION_LAT);
            } else {
                fail("Error: the id you used to query returned an empty cursor");
            }

            // there should be no more records
            assertFalse("Error: there should be only one record returned from a location query",
                    locationCursor.moveToNext());

            // add the location again
            long newLocationId = fwt.addLocation(ADD_LOCATION_ID, ADD_LOCATION_NAME, ADD_LOCATION_MUNICIPALITY,
                    ADD_LOCATION_PROVINCE, ADD_LOCATION_TYPE, ADD_LOCATION_LAT, ADD_LOCATION_LON);

            assertEquals("Error: inserting a location again should return the same ID",
                    indiceBd, newLocationId);
        }
        // reset our state back to normal
        getContext().getContentResolver().delete(WeatherContract.LocationEntry.CONTENT_URI,
                WeatherContract.LocationEntry._ID + " = ?",
                new String[]{ADD_LOCATION_ID.toString()});
       // clean up the test so that other tests can use the content provider
        getContext().getContentResolver().
                acquireContentProviderClient(WeatherContract.LocationEntry.CONTENT_URI).
                getLocalContentProvider().shutdown();
    }
}
